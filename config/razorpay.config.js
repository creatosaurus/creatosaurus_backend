const Razorpay = require('razorpay')
const constant = require("../utils/constant")

const createReazorPayInstance = () => {
    return new Razorpay({
        key_id: constant.rzp_key_id,
        key_secret: constant.rzp_key_secret
    })
}

module.exports = {
    createReazorPayInstance
}

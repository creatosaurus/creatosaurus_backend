const express = require('express');
const router = express.Router();
const checkAuth = require("../../Middleware/CheckAuth");
const { upgradeToPaidPlan } = require('../../Functions/featureSettings');

router.post("/upgrade", checkAuth, async (req, res) => {
    try {
        const userId = req.body.userId
        const planId = "6247081137b553074fb6b258" // teams plan id
        let duration = req.body.duration

        if (duration === "3") {
            duration = "3 month"
        } else if (duration === "6") {
            duration === "6 month"
        } else {
            duration = "1 month"
        }

        await upgradeToPaidPlan(userId, planId, duration)
        res.send("done")
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: 'Internal server error'
        })
    }
})

module.exports = router;
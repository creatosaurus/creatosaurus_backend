const express = require('express')
const router = express.Router()
const axios = require("axios")
const checkAuth = require("../Middleware/CheckAuth")
let token = ""

const getToken = async () => {
    try {
        const response = await axios.get("https://www.universal-tutorial.com/api/getaccesstoken", {
            headers: {
                "Accept": "application/json",
                "api-token": "Zv17rsaDTgQWTHgIbQDo0QDZlcEP2NgTPWgHifX6q2bu2jN0E3BqumA7HMxz4qIaJCU",
                "user-email": "mayurgaikwad7474@gmail.com"
            }
        })

        token = response.data.auth_token
        return null
    } catch (error) {
        return null
    }
}

router.get("/country", checkAuth, async (req, res) => {
    try {
        if (!token) {
            await getToken()
        }

        const response = await axios.get("https://www.universal-tutorial.com/api/countries/", {
            headers: {
                "Authorization": `Bearer ${token}`,
                "Accept": "application/json"
            }
        })

        res.send(response.data)
    } catch (error) {
        res.status({ error: "internal server error" })
        console.log(error)
    }
})

router.get("/state", checkAuth, async (req, res) => {
    try {
        if (!token) {
            await getToken()
        }

        const country = req.query.country
        const response = await axios.get(`https://www.universal-tutorial.com/api/states/${country}`, {
            headers: {
                "Authorization": `Bearer ${token}`,
                "Accept": "application/json"
            }
        })

        res.send(response.data)
    } catch (error) {
        res.status({ error: "internal server error" })
        console.log(error)
    }
})

router.get("/city", checkAuth, async (req, res) => {
    try {
        if (!token) {
            await getToken()
        }

        const state = req.query.state
        const response = await axios.get(`https://www.universal-tutorial.com/api/cities/${state}`, {
            headers: {
                "Authorization": `Bearer ${token}`,
                "Accept": "application/json"
            }
        })

        res.send(response.data)
    } catch (error) {
        res.status({ error: "internal server error" })
        console.log(error)
    }
})

const getCountryStateCity = (data) => {
    let country = '';
    let state = '';
    let address = '';

    if (data.results && data.results.length > 0) {
        address = data.results[0].formatted_address
        const addressComponents = data.results[0].address_components;

        addressComponents.forEach(component => {
            if (component.types.includes("country")) {
                country = component.long_name;
            }

            if (component.types.includes("administrative_area_level_1")) {
                state = component.long_name;
            }
        });
    }

    return { country, state, address };
}

router.get("/code", checkAuth, async (req, res) => {
    try {
        const code = req.query.code
        const response = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${code}&key=AIzaSyANQytBHQP7WpQ5OI2PnL66ocRZsdWjTv0`)
        const locationDetails = getCountryStateCity(response.data);
        res.send(locationDetails)
    } catch (error) {
        res.status({ error: "internal server error" })
        console.log(error)
    }
})

module.exports = router
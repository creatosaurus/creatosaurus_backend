const express = require('express');
const router = express.Router();
const workspace = require('../Schema/Workspace')
const user = require("../Schema/User")

router.get("/:workspaceId", async (req, res) => {
    try {
        const data = await workspace.findById(req.params.workspaceId)

        const userInfo = data.team.map(async userData => {
            const data = await user.findOne({ email: userData.user_email })
            return data._id
        })

        let userData = await Promise.all(userInfo);

        res.send(userData)
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: 'Internal server error'
        })
    }
})

module.exports = router;
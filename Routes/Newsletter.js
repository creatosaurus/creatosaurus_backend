const express = require('express')
const router = express.Router()
const newsletter = require('../Schema/Newsletter')
const user = require("../Schema/User")
const logger = require("../logger")
const nodemailer = require('nodemailer');
const sesTransport = require('nodemailer-ses-transport');
const appsumo = require('../Schema/AppSumo')

const transporter = nodemailer.createTransport(
    sesTransport({
        accessKeyId: 'AKIAQ5RO3HORQD7AMD4E',
        secretAccessKey: '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',
        region: 'ap-south-1',
        rateLimit: 5,
    })
);

const sendEmail = async (subject, emails, html) => {
    const mailOptionsArray = emails.map((email) => ({
        from: 'Creatosaurus <account@creatosaurus.io>',
        to: email,
        subject: subject,
        html: html,
    }));

    const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
    const sendDelay = 1000 / 200; // ses supports 200 emails per second

    try {
        for (const mailOptions of mailOptionsArray) {
            try {
                await transporter.sendMail(mailOptions);
                await delay(sendDelay); // Add a delay between sending each email
            } catch (error) {
                console.log(error);
            }
        }
    } catch (error) {
        console.log(error);
    }
};

router.post("/send", async (req, res) => {
    try {
        let emails = []
        if (req.body.type === "test") {
            emails = [req.body.testEmail]
        } else if (req.body.type === "all") {
            const newsletterEmails = await newsletter.find({}, { email: 1 })
            const userEmails = await user.find({}, { email: 1 })

            emails = [...newsletterEmails, ...userEmails].map(data => {
                return data.email
            })

            emails = [...new Set(emails)];
        } else if (req.body.type === "all-email-newsletter-yes") {
            const newsletterEmails = await newsletter.find({ isSubscribed: true }, { email: 1 })
            const userEmails = await user.find({ sendEmail: { $ne: false } }, { email: 1 });

            emails = [...newsletterEmails, ...userEmails].map(data => {
                return data.email
            })

            emails = [...new Set(emails)];
        } else if (req.body.type === "email-yes") {
            const userEmails = await user.find({ sendEmail: { $ne: false } }, { email: 1 });
            emails = userEmails.map(data => {
                return data.email
            })
        } else if (req.body.type === "newsletter-yes") {
            const newsletterEmails = await newsletter.find({ isSubscribed: true }, { email: 1 })
            emails = newsletterEmails.map(data => {
                return data.email
            })
        } else if (req.body.type === "email-no") {
            const userEmails = await user.find({ sendEmail: false }, { email: 1 });
            emails = userEmails.map(data => {
                return data.email
            })
        } else if (req.body.type === "newsletter-no") {
            const newsletterEmails = await newsletter.find({ isSubscribed: false }, { email: 1 })
            emails = newsletterEmails.map(data => {
                return data.email
            })
        } else if (req.body.type === "email-all") {
            const userEmails = await user.find({}, { email: 1 });
            emails = userEmails.map(data => {
                return data.email
            })
        } else if (req.body.type === "newsletter-all") {
            const newsletterEmails = await newsletter.find({}, { email: 1 })
            emails = newsletterEmails.map(data => {
                return data.email
            })
        } else if (req.body.type === "app-sumo") {
            const appsumoEmails = await appsumo.find({ planId: { $ne: "free" } }, { activationEmail: 1 })
            emails = appsumoEmails.map(data => {
                return data.activationEmail
            })

            emails = [...new Set(emails)];
        } else if (req.body.type === "custom") {
            emails = [...new Set(req.body.customEmails)]
        } else if (req.body.type === "all-email-without-appsumo") {
            const userEmails = await user.find({ sendEmail: { $ne: false } }, { email: 1, _id: 0 })
            const appsumoEmails = await appsumo.find({ planId: { $ne: "free" } }, { activationEmail: 1 })

            let newEmails = userEmails.map(data => data.email)
            let newAppSumoEmails = appsumoEmails.map(data => data.activationEmail)
            emails = [...newEmails, ...newAppSumoEmails]
            emails = emails.filter(data => !newAppSumoEmails.includes(data))
        }

        sendEmail(req.body.subject, emails, req.body.htmlPage)
        res.send("done")
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: 'Internal server error'
        });
    }
})

// @Route /creatosaurus/newsletter/subscribe
// @desc create a newsletter subscription
router.post('/subscribe', async (req, res) => {
    try {
        const existingSubscription = await newsletter.findOne({ email: req.body.email });

        if (existingSubscription) {
            return res.status(200).json({
                message: "Successfully subscribed to newsletter"
            });
        } else {
            await newsletter.create(req.body);
        }

        res.status(200).json({
            message: "Successfully subscribed to newsletter"
        });
    } catch (error) {
        logger.error(error);
        res.status(500).json({
            error: 'Internal server error'
        });
    }
});

// @Route /creatosaurus/newsletter/subscriptions
// @desc get all newsletter subscription
router.get('/subscriptions', async (req, res) => {
    try {
        const data = await newsletter.find({})
        res.status(200).json({
            subscriptions: data
        });
    } catch (error) {
        logger.error(error);
        res.status(500).json({
            error: 'Internal server error'
        });
    }
})

// @Route /creatosaurus/newsletter/subscriber/:email
// @desc get a newsletter subscriber by email
router.get('/subscriber/:email', async (req, res) => {
    try {
        const data = await newsletter.findOne({ email: req.params.email });
        if (data !== null) {
            return res.status(200).json({
                subscriber: data
            });
        } else {
            return res.status(404).json({
                message: "Subscriber no found."
            });
        }

    } catch (error) {
        logger.error(error);
        res.status(500).json({
            error: 'Internal server error'
        });
    }
})


module.exports = router
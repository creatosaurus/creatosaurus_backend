const express = require('express')
const router = express.Router()
const couponCode = require('../Schema/CouponCode')
const checkAuth = require('../Middleware/CheckAuth')
const logger = require("../logger")

// @Route /creatosaurus/coupon/code/create
// @desc create coupon code
router.post('/create', checkAuth, async (req, res) => {
    try {
        const data = await couponCode.create(req.body)
        res.send(data)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: 'internal server error'
        })
    }
})


router.get('/find', checkAuth, async (req, res) => {
    try {
        let code = req.query.code.toLocaleLowerCase()
        const data = await couponCode.findOne({ code: code })

        if (data === null) return res.status(400).json({ error: 'invalid coupon code' })
        if (data.used === true) return res.status(400).json({ error: 'coupon code has been used' })

        const couponCodeExpirationDate = new Date(data.expireDate)
        const currentDate = new Date()

        if (currentDate <= couponCodeExpirationDate) {
            res.send(data)
        } else {
            res.status(400).json({
                error: 'code expired'
            })
        }
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: 'internal server error'
        })
    }
})

module.exports = router
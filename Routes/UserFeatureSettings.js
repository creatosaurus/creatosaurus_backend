const express = require('express');
const router = express.Router();
const workspace = require('../Schema/Workspace')
const checkAuth = require('../Middleware/CheckAuth')
const logger = require("../logger");
const UserFeatureSettings = require('../Schema/UserFeatureSettings');

// @POST creatosaurus/userfeaturefactory/captionator/credits
// @desc check user credit remian for the user or not and also subtract remain credite's
// Using in captionator project
router.post('/captionator/credits', checkAuth, async (req, res) => {
    try {
        const userfeaturefactoryData = await UserFeatureSettings.findOne({ userId: req.body.workspaceOwnerUserId }, { aiCredits: 1 })
        if (userfeaturefactoryData.aiCredits === null) return res.status(200).json({ message: "unlimited" })
        res.status(200).json({ message: userfeaturefactoryData.aiCredits })
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: 'internal server error'
        })
    }
})

// @POST creatosaurus/userfeaturefactory/captionator/remove/credit
// @desc check user credit remian for the user or not and also subtract remain credite's
// Using in captionator project
router.post('/captionator/remove/credit', checkAuth, async (req, res) => {
    try {
        const userfeaturefactoryData = await UserFeatureSettings.findOne({ userId: req.body.workspaceOwnerUserId }, { aiCredits: 1 })

        if (userfeaturefactoryData.aiCredits === null) return res.status(200).json({ message: 'unlimited' })

        let remainCount = parseInt(userfeaturefactoryData.aiCredits) - parseInt(req.body.removeCredits)
        userfeaturefactoryData.aiCredits = remainCount < 0 ? 0 : remainCount
        await userfeaturefactoryData.save()
        res.status(200).json({ message: userfeaturefactoryData.aiCredits })
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: 'internal server error'
        })
    }
})

// @POST creatosaurus/userfeaturefactory/ai-image/check/credit
// @desc check user credit remian for the user or not and also subtract remain credite's
// Using in captionator project
router.post('/ai-image/check/credit', checkAuth, async (req, res) => {
    try {
        const userfeaturefactoryData = await UserFeatureSettings.findOne({ userId: req.body.workspaceOwnerUserId }, { aiImageCredits: 1 })
        if (!userfeaturefactoryData.aiImageCredits) return res.status(200).json({ message: 0 })

        if (userfeaturefactoryData?.aiImageCredits === null) return res.status(200).json({ message: 'unlimited' })
        res.status(200).json({ message: parseInt(userfeaturefactoryData.aiImageCredits) })
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: 'internal server error'
        })
    }
})

// @POST creatosaurus/userfeaturefactory/ai-image/remove/credit
// @desc check user credit remian for the user or not and also subtract remain credite's
// Using in captionator project
router.post('/ai-image/remove/credit', checkAuth, async (req, res) => {
    try {
        const { workspaceOwnerUserId, removeCredit } = req.body;

        // Validate request body
        if (!workspaceOwnerUserId) {
            return res.status(400).json({ error: 'workspaceOwnerUserId is required' });
        }

        // Fetch user feature settings
        const userFeatureData = await UserFeatureSettings.findOne(
            { userId: workspaceOwnerUserId },
            { aiImageCredits: 1 }
        );

        if (!userFeatureData?.aiImageCredits) return res.status(200).json({ message: 0 })

        // Handle unlimited credits case
        if (userFeatureData.aiImageCredits === null) {
            return res.status(200).json({ message: 'unlimited' });
        }

        // Calculate remaining credits
        const creditsToRemove = parseInt(removeCredit) || 1;
        userFeatureData.aiImageCredits = Math.max(userFeatureData.aiImageCredits - creditsToRemove, 0);

        // Save updated user feature settings
        await userFeatureData.save();

        res.status(200).json({ remainingCredits: userFeatureData.aiImageCredits });
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: 'internal server error'
        })
    }
})

// @POST creatosaurus/userfeaturefactory/cache/remove/credit/account
// @desc check user credit remian for the user or not and also subtract remain credite's
// Using in cache project
router.post('/cache/remove/credit/account', checkAuth, async (req, res) => {
    try {
        const workspaceUserData = await workspace.findById(req.body.id)
        const userfeaturefactoryData = await UserFeatureSettings.findOne({ userId: workspaceUserData.user_id }, { socialAccounts: 1 })
        if (userfeaturefactoryData.socialAccounts === null) return res.status(200).json({
            accountLimitToConnect: 'unlimited',
            limitIsThere: true
        })

        if (parseInt(userfeaturefactoryData.socialAccounts) - req.body.accountsToConnect < 0) return res.status(400).json({
            error: "credit's over upgrade to connect more accounts."
        })

        userfeaturefactoryData.socialAccounts = parseInt(userfeaturefactoryData.socialAccounts) - req.body.accountsToConnect
        await userfeaturefactoryData.save()
        res.status(200).json({
            accountLimitToConnect: userfeaturefactoryData.socialAccounts,
            limitIsThere: true
        })
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: 'internal server error'
        })
    }
})

// @POST creatosaurus/userfeaturefactory/cache/add/credit/account
// @desc check user credit remian for the user or not and also subtract remain credite's
// Using in cache project
router.post('/cache/add/credit/account', checkAuth, async (req, res) => {
    try {
        const workspaceUserData = await workspace.findById(req.body.id)
        const userfeaturefactoryData = await UserFeatureSettings.findOne({ userId: workspaceUserData.user_id }, { socialAccounts: 1 })
        if (userfeaturefactoryData.socialAccounts === null) return res.status(200).json({
            accountLimitToConnect: 'unlimited',
            limitIsThere: true
        })

        userfeaturefactoryData.socialAccounts = parseInt(userfeaturefactoryData.socialAccounts) + 1
        await userfeaturefactoryData.save()
        res.status(200).json({
            accountLimitToConnect: userfeaturefactoryData.socialAccounts,
            limitIsThere: true
        })
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: 'internal server error'
        })
    }
})

// @POST creatosaurus/userfeaturefactory/cache/remove/credit/queue
// @desc check user credit remian for the user or not and also subtract remain credite's
// Using in captionator project
router.post('/cache/remove/credit/queue', checkAuth, async (req, res) => {
    try {
        const workspaceUserData = await workspace.findById(req.body.id)
        const userfeaturefactoryData = await UserFeatureSettings.findOne({ userId: workspaceUserData.user_id }, { schedulePost: 1 })
        if (userfeaturefactoryData.schedulePost === null) return res.status(200).json({
            accountLimitToConnect: 'unlimited',
            limitIsThere: true
        })

        if (parseInt(userfeaturefactoryData.schedulePost) <= 0) return res.status(400).json({
            error: "credit's over upgrade to add more post to queue."
        })

        userfeaturefactoryData.schedulePost = parseInt(userfeaturefactoryData.schedulePost) - 1
        await userfeaturefactoryData.save()
        res.status(200).json({
            accountLimitToConnect: userfeaturefactoryData.schedulePost,
            limitIsThere: true
        })
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: 'internal server error'
        })
    }
})

// @POST creatosaurus/userfeaturefactory/cache/add/credit/queue
// @desc check user credit remian for the user or not and also subtract remain credite's
// Using in cache project
router.post('/cache/add/credit/queue', checkAuth, async (req, res) => {
    try {
        const workspaceUserData = await workspace.findById(req.body.id)
        const userfeaturefactoryData = await UserFeatureSettings.findOne({ userId: workspaceUserData.user_id }).populate('planId')
        if (userfeaturefactoryData.schedulePost === null) return res.status(200).json({
            accountLimitToConnect: 'unlimited',
            limitIsThere: true
        })

        if (parseInt(userfeaturefactoryData.schedulePost) >= parseInt(userfeaturefactoryData.planId.schedulePost)) {
            userfeaturefactoryData.schedulePost = userfeaturefactoryData.planId.schedulePost
            await userfeaturefactoryData.save()
            return res.status(200).json({
                accountLimitToConnect: userfeaturefactoryData.planId.schedulePost,
                limitIsThere: true
            })
        }

        userfeaturefactoryData.schedulePost = parseInt(userfeaturefactoryData.schedulePost) + 1
        await userfeaturefactoryData.save()
        res.status(200).json({
            accountLimitToConnect: userfeaturefactoryData.schedulePost,
            limitIsThere: true
        })
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: 'internal server error'
        })
    }
})

// @POST creatosaurus/userfeaturefactory/secret/creatosaurus/add/credit/account
// @desc check user credit remian for the user or not and also subtract remain credite's
// Using in cache project
router.post('/secret/creatosaurus/add/credit/account', async (req, res) => {
    try {
        const workspaceUserData = await workspace.findById(req.body.id)
        const userfeaturefactoryData = await UserFeatureSettings.findOne({ userId: workspaceUserData.user_id }, { schedulePost: 1 })
        if (userfeaturefactoryData.schedulePost === null) return res.status(200).json({
            accountLimitToConnect: 'unlimited',
            limitIsThere: true
        })

        userfeaturefactoryData.schedulePost = parseInt(userfeaturefactoryData.schedulePost) + 1
        await userfeaturefactoryData.save()
        res.status(200).json({
            accountLimitToConnect: userfeaturefactoryData.schedulePost,
            limitIsThere: true
        })
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: 'internal server error'
        })
    }
})


/**
 * // @POST creatosaurus/userfeaturefactory/hashtag/remove/credit
// @desc check user credit remian for the user or not and also subtract remain credite's
// Using in captionator project
router.post('/hashtag/remove/credit', checkAuth, async (req, res) => {
    try {
        const workspaceUserData = await workspace.findById(req.body.id)
        const userfeaturefactoryData = await userfeaturefactory.findOne({ userId: workspaceUserData.user_id })
        if (userfeaturefactoryData.hashtagSearch === "unlimited") return res.status(200).json({ message: 'unlimited' })
        if (userfeaturefactoryData.hashtagSearch === "0") return res.status(400).json({ error: 'credit over' })
        userfeaturefactoryData.hashtagSearch = parseInt(userfeaturefactoryData.hashtagSearch) - 1
        await userfeaturefactoryData.save()
        res.status(200).json({ message: userfeaturefactoryData.hashtagSearch })
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: 'internal server error'
        })
    }
})

// @POST creatosaurus/userfeaturefactory/hashtag/remove/credit/instagram
// @desc check user credit remian for the user or not and also subtract remain credite's
// Using in captionator project
router.post('/hashtag/remove/credit/instagram', checkAuth, async (req, res) => {
    try {
        const workspaceUserData = await workspace.findById(req.body.id)
        const userfeaturefactoryData = await userfeaturefactory.findOne({ userId: workspaceUserData.user_id })
        if (userfeaturefactoryData.hashtagSearchInstagram === "unlimited") return res.status(200).json({ message: 'unlimited' })
        if (parseInt(userfeaturefactoryData.hashtagSearchInstagram) <= 0) return res.status(400).json({ error: 'credit over' })
        userfeaturefactoryData.hashtagSearchInstagram = parseInt(userfeaturefactoryData.hashtagSearchInstagram) - 1
        await userfeaturefactoryData.save()
        res.status(200).json({ message: userfeaturefactoryData.hashtagSearchInstagram })
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: 'internal server error'
        })
    }
})
 */

module.exports = router
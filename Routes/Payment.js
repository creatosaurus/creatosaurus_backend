const express = require('express');
const router = express.Router();
const payment = require("../Schema/Payment")
const checkAuth = require('../Middleware/CheckAuth')

router.get("/", checkAuth, async (req, res) => {
    try {
        const data = await payment
            .find({ userId: req.body.userId }, { userMetaData: 0 })
            .populate("planId", { planName: 1 })
            .sort({ createdAt: -1 }); // Sort by latest

        res.send(data)
    } catch (error) {
        res.status(500).json({
            error: 'internal server error'
        })
    }
})

module.exports = router
const express = require("express")
const router = express.Router()
const constant = require("../utils/constant");
const CheckAuth = require("../Middleware/CheckAuth");
const stripe = require("stripe")(constant.stripe_key_secret, { apiVersion: "2022-08-01" });
const payment = require("../Schema/Payment.js")
const plan = require("../Schema/Plan.js");
const order = require("../Schema/Order.js")
const couponCode = require("../Schema/CouponCode.js")
const { upgradeToPaidPlan } = require("../Functions/featureSettings.js");

router.post("/payment", express.json(), CheckAuth, async (req, res) => {
    try {
        // Fetch plan details
        const planDetails = await plan.findOne({ _id: req.body.id });
        if (!planDetails) return res.status(404).json({ error: "Plan not found" });

        // Fetch coupon data if provided
        let discount = null;
        if (req.body.code) {
            const couponCodeData = await couponCode.findOne({ _id: req.body.code });
            if (!couponCodeData) return res.status(404).json({ error: "Coupon code not found" });
            discount = couponCodeData.discount;
        }

        // Calculate the price based on duration and currency
        const currency = "USD"
        let price = req.body.duration === "monthly" ? planDetails.monthlyPriceUSD : planDetails.yearlyPriceUSD;

        // calculate discount
        if (discount) {
            let discountAmount = (price * discount) / 100;
            price = price - discountAmount;
        }

        const data = {
            userId: req.body.userId,
            couponCode: req.body.code,
            currency: currency,
            userDetails: { ...req.body.userDetails },
            companyDetails: { ...req.body.companyDetails },
            planInfo: {
                planId: req.body.id,
                duration: req.body.duration,
            }
        }

        const orderData = await order.create(data)
        const session = await stripe.checkout.sessions.create({
            line_items: [
                {
                    price_data: {
                        currency: currency,
                        product_data: {
                            name: planDetails.planName,
                        },
                        unit_amount: price * 100,
                    },
                    quantity: 1,
                },
            ],
            mode: 'payment',
            metadata: {
                userId: req.body.userId,
                orderId: orderData._id.toString()
            },
            client_reference_id: req.body.userId,
            success_url: `https://www.creatosaurus.io/pricing?id=${req.body.id}`,
            cancel_url: `https://www.creatosaurus.io/pricing?failed=true`,
        });

        res.send(session.url)
    } catch (error) {
        res.status(500).json({ error: "Internal server error" })
        console.log(error)
    }
})


const addDuration = (duration) => {
    const currentDate = new Date();

    if (duration === "month") {
        currentDate.setMonth(currentDate.getMonth() + 1); // Adds one month accurately
    } else if (duration === "year") {
        currentDate.setFullYear(currentDate.getFullYear() + 1); // Adds one year accurately
    }

    return currentDate.getTime(); // Returns timestamp
};


// Utility function to generate invoice number
const generateInvoiceNumber = (count) => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth(); // January is 0, so March is 2

    let financialYear;
    if (currentMonth >= 2) {
        financialYear = `${currentYear.toString().slice(-2)}${(currentYear + 1).toString().slice(-2)}`;
    } else {
        financialYear = `${(currentYear - 1).toString().slice(-2)}${currentYear.toString().slice(-2)}`;
    }

    const number = count + 1
    const invoiceNumber = `C-${financialYear}-${number}`;
    return invoiceNumber;
}

router.post("/webhook", express.raw({ type: 'application/json' }), async (req, res) => {
    try {
        const sig = req.headers['stripe-signature'];
        const event = stripe.webhooks.constructEvent(req.body, sig, constant.stripe_webhook_secret);

        if (event.type === "checkout.session.completed") {
            const { metadata } = event.data.object
            const orderData = await order.findOne({ _id: metadata.orderId })
            const planDetails = await plan.findOne({ _id: orderData.planInfo.planId })

            // Get discount information if available
            let discount = null;
            if (orderData.couponCode) {
                const couponCodeData = await couponCode.findOne({ _id: orderData.couponCode });
                if (!couponCodeData) return res.status(404).json({ error: "Coupon code not found" });
                discount = couponCodeData.discount;
            }

            // Calculate the price based on duration and currency
            let price = orderData.planInfo.duration === "monthly" ? planDetails.monthlyPriceUSD : planDetails.yearlyPriceUSD;
            const originalAmount = price

            // calculate discount
            if (discount) {
                let discountAmount = (price * discount) / 100;
                price = price - discountAmount;
            }

            const finalAmount = price

            const count = await payment.countDocuments({})
            const invoiceId = generateInvoiceNumber(count); // Generate unique invoice number

            // Subscription dates logic
            const subscriptionStartDate = new Date(); //
            const subscriptionEndDate = new Date(subscriptionStartDate);
            if (orderData.planInfo.duration === "monthly") {
                subscriptionEndDate.setMonth(subscriptionEndDate.getMonth() + 1); // Add one month
            } else { // yearly
                subscriptionEndDate.setFullYear(subscriptionEndDate.getFullYear() + 1); // Add one year
            }

            const paymentDetails = {
                userId: orderData.userId,
                planId: orderData.planInfo.planId,
                invoiceId: invoiceId,
                paymentProvider: "stripe",
                paymentMethod: "card",
                paymentStatus: 'completed',
                originalAmount: originalAmount,
                finalAmount: finalAmount,
                currency: orderData.currency,
                discountDetails: {
                    discountPercentage: discount,
                    couponCode: orderData.couponCode,
                },
                subscriptionDetails: {
                    subscriptionType: orderData.planInfo.duration,
                    subscriptionStartDate: subscriptionStartDate,
                    subscriptionEndDate: subscriptionEndDate,
                    autoRenew: false
                },
                paymentMetaData: {
                    ...event.data.object
                },
                userMetaData: {
                    userDetails: { ...orderData.userDetails },
                    companyDetails: { ...orderData.companyDetails }
                }
            }

            await payment.create(paymentDetails)
            upgradeToPaidPlan(orderData.userId, orderData.planInfo.planId, orderData.planInfo.duration)
        }

        // Return a 200 response to acknowledge receipt of the event
        res.send();
    } catch (error) {
        console.log(error)
        res.status(400).send(`Webhook Error: ${error.message}`);
    }
})

module.exports = router
const express = require('express');
const router = express.Router();
const payment = require("../Schema/Payment")

router.get('/:invoiceId', async (req, res) => {
    try {
        const transactionDetails = await payment.findOne({ invoiceId: req.params.invoiceId }).populate("planId")
        if (!transactionDetails) return res.status(404).json({ error: "payment not found" })
        res.send(transactionDetails)
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            error: error
        })
    }
})


module.exports = router

const express = require('express');
const router = express.Router();
const checkAuth = require('../Middleware/CheckAuth')
const UserFeatureSettings = require('../Schema/UserFeatureSettings');
const workspace = require('../Schema/Workspace')
const logger = require("../logger")

const {
    CreateWorkspace,
    deleteWorkspace,
    updateWorkspace,
    viewWorkspace,
    getAllWorkspaceAsPerUserID,
    getAllworkspace,
    ConfirmWorkspace,
    inviteTeamMembers,
    updateMemberPermission,
    removeMemberFromWorkspace,
    updateWorkspaceName,
    getUserTimeZone
} = require('../Controller/Workspace');
const User = require('../Schema/User');

router.put('/activate/user', checkAuth, async (req, res) => {
    try {
        const featureFactoryData = await UserFeatureSettings.findOne({ userId: req.body.userId })
        const workspaceData = await workspace.find({ user_id: req.body.userId })

        let emails = []
        workspaceData.forEach(data => {
            data.team.forEach(team => {
                if (team.active === true) {
                    emails.push(team.user_email)
                }
            })
        })
        emails = [...new Set(emails)]

        // check whether user is allready activated
        const activated = await workspace.find({ 'team.user_email': req.body.email, 'team.active': true, userId: req.body.userId })

        if (emails.length < parseInt(featureFactoryData.users) || activated.length > 0) {
            const userInWorkspaces = await workspace.findById(req.body.id)
            userInWorkspaces.team = userInWorkspaces.team.map(data => {
                if (data.user_email === req.body.email) {
                    data.active = true
                }
                return data
            })

            const updateduserInWorkspaces = await userInWorkspaces.save()
            res.send(updateduserInWorkspaces)
        } else {
            res.status(400).json({
                error: `You have ${featureFactoryData.users} users limit upgrade to add more users`
            })
        }
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: `Internal server error`
        })
    }
})

router.post('/createWorkspace', checkAuth, CreateWorkspace)

router.delete('/deleteWorkspace/:id/:userId', deleteWorkspace)

router.put('/updateWorkspace', updateWorkspace)

router.post('/viewWorkspace', viewWorkspace)

router.get('/getAllWorkspaceAsPerUserid/:user_id/:email', getAllWorkspaceAsPerUserID)

router.get('/getAllworkspace', getAllworkspace)

// remove after 10 days current day 29 nov
router.get('/confirm/:id/:email', checkAuth, ConfirmWorkspace)

router.put('/inviteTeamMembers', checkAuth, inviteTeamMembers)

router.put('/changeMemberPermission', updateMemberPermission)

router.post('/removeMemberFromWorkspace', removeMemberFromWorkspace)

router.put('/updateWorkspaceName', updateWorkspaceName)

router.post('/getUserTimezone', getUserTimeZone)

module.exports = router;
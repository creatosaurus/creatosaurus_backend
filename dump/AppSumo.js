const express = require('express')
const router = express.Router()
const checkAuth = require("../Middleware/CheckAuth")
const jwt = require('jsonwebtoken');
const keySecret = 'kv1IDb96qqZnc0w0AINeNkCxvbqJsdDLklu76eKpIjY';
const appSumo = require('../Schema/AppSumo')
const user = require('../Schema/User')
const plan = require('../Schema/Plan')
const userFeatureFactory = require('../Schema/UserFeatureFactory')
const appSumoCode = require('../Schema/AppSumoCodes')
const { v4: uuidv4 } = require('uuid');

router.post("/auth", async (req, res) => {
    try {
        userName = "creatosaurus@appsumo"
        secret = `jQP$99!4mc%&^%Q&eueK0qPW*03T`

        if (req.body.username === userName && req.body.password === secret) {
            const token = jwt.sign({ id: "97d19378-d378-421c-b3f7-482854bab7f4" }, keySecret, { expiresIn: `1y` });
            res.status(200).json({ access: token });
        } else {
            res.status(401).json({ error: "Invalid username or password" });
        }
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Internal server error" });
    }
})


router.post("/notify", checkAuth, async (req, res) => {
    try {
        let { activation_email, uuid, action, plan_id, invoice_item_uuid } = req.body;

        if (action === "refund") {
            plan_id = "free"
        } else if (plan_id === "creatosaurus_tier1") {
            plan_id = "creatosaurus appsumo tier 1"
        } else if (plan_id === "creatosaurus_tier2") {
            plan_id = "creatosaurus appsumo tier 2"
        } else {
            plan_id = "creatosaurus appsumo tier 3"
        }

        const appSumoUser = await appSumo.findOne({ activationEmail: activation_email, appSumoUserId: uuid });

        if (appSumoUser !== null && action === "activate") {
            return res.status(400).json({ error: 'Duplicate email for the activation' });
        }

        if (appSumoUser === null && action === "activate") {
            // Create new user
            const userExists = await user.findOne({ email: activation_email });

            if (userExists === null) {
                const userName = activation_email.trim().slice(0, -10);
                const userData = await user.create({
                    email: activation_email,
                    userName: userName,
                    emailVerified: true,
                });

                let userIsThere = false
                await createAppSumoUserAndFeatures(userData._id, uuid, plan_id, activation_email, invoice_item_uuid, userIsThere);
            } else {
                let userIsThere = true
                await createAppSumoUserAndFeatures(userExists._id, uuid, plan_id, userExists.email, invoice_item_uuid, userIsThere);
            }

            res.status(201).json({
                message: "product activated",
                redirect_url: `https://www.creatosaurus.io/?source=appsumo&email=${activation_email}`
            })
        } else if (action === "enhance_tier" || action === "reduce_tier" || action === "refund") {
            appSumoUser.planId = plan_id
            if (action === "refund") {
                appSumoUser.invoiceId = invoice_item_uuid
            }
            await appSumoUser.save()

            const userExists = await user.findOne({ email: activation_email });
            const freePlan = await plan.findOne({ planName: plan_id });
            let userFeatureFactoryData = await userFeatureFactory.findOne({ userId: userExists._id })

            userFeatureFactoryData.planId = freePlan._id
            userFeatureFactoryData.workspace = freePlan.workspace
            userFeatureFactoryData.users = freePlan.users
            userFeatureFactoryData.socialAccounts = freePlan.socialAccounts
            userFeatureFactoryData.schedulePost = freePlan.schedulePost
            userFeatureFactoryData.aiCredites = freePlan.aiCredites
            userFeatureFactoryData.hashtagSearch = freePlan.hashtagSearch
            userFeatureFactoryData.hashtagSearchInstagram = freePlan.hashtagSearchInstagram
            userFeatureFactoryData.startDate = new Date()
            userFeatureFactoryData.endDate = action === "refund" ? Date.now() + 30 * 24 * 60 * 60 * 1000 : Date.now() + 36500 * 24 * 60 * 60 * 1000
            userFeatureFactoryData.reCreditDate = action === "refund" ? null : Date.now() + 30 * 24 * 60 * 60 * 1000
            await userFeatureFactoryData.save()

            res.status(200).json({
                message: action === "enhance_tier" ? "product enhanced" : action === "refund" ? "product refunded" : "product reduced"
            })
        } else if (action === "update") {
            appSumoUser.invoiceId = invoice_item_uuid
            await appSumoUser.save()

            res.status(200).json({
                message: "product updated"
            })
        } else {
            res.status(400).json({
                error: "Bad request"
            })
        }
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: "Internal server error"
        })
    }
})


async function createAppSumoUserAndFeatures(userId, appSumoUserId, planId, activationEmail, invoiceId, userIsThere) {
    await appSumo.create({
        userId: userId,
        appSumoUserId: appSumoUserId,
        planId: planId,
        activationEmail: activationEmail,
        invoiceId: invoiceId
    });

    const freePlan = await plan.findOne({ planName: planId });

    if (userIsThere) {
        let userFeatureFactoryData = await userFeatureFactory.findOne({ userId: userId })
        userFeatureFactoryData.planId = freePlan._id
        userFeatureFactoryData.workspace = freePlan.workspace
        userFeatureFactoryData.users = freePlan.users
        userFeatureFactoryData.socialAccounts = freePlan.socialAccounts
        userFeatureFactoryData.schedulePost = freePlan.schedulePost
        userFeatureFactoryData.aiCredites = freePlan.aiCredites
        userFeatureFactoryData.hashtagSearch = freePlan.hashtagSearch
        userFeatureFactoryData.hashtagSearchInstagram = freePlan.hashtagSearchInstagram
        userFeatureFactoryData.startDate = new Date()
        userFeatureFactoryData.endDate = Date.now() + 36500 * 24 * 60 * 60 * 1000
        userFeatureFactoryData.reCreditDate = Date.now() + 30 * 24 * 60 * 60 * 1000
        await userFeatureFactoryData.save()
    } else {
        await userFeatureFactory.create({
            planId: freePlan._id,
            userId: userId,
            workspace: freePlan.workspace,
            users: freePlan.users,
            socialAccounts: freePlan.socialAccounts,
            schedulePost: freePlan.schedulePost,
            aiCredites: freePlan.aiCredites,
            hashtagSearch: freePlan.hashtagSearch,
            hashtagSearchInstagram: freePlan.hashtagSearchInstagram,
            startDate: new Date(),
            endDate: Date.now() + 36500 * 24 * 60 * 60 * 1000,
            reCreditDate: Date.now() + 30 * 24 * 60 * 60 * 1000
        })
    }
}


// router.get('/create/code', async (req, res) => {
//     try {
//         const numberOfCoupons = 10000;
//         const couponCodes = [];

//         for (let i = 0; i < numberOfCoupons; i++) {
//             couponCodes.push({
//                 code: uuidv4(),
//                 isUsed: false,
//                 usedBy: '',
//                 dateUsed: '',
//             });
//         }


//         const response = await appSumoCode.insertMany(couponCodes);

//         res.status(200).json({ success: true, insertedCount: response.length });
//     } catch (error) {
//         console.error(error);
//         res.status(500).json({ success: false, error: 'Internal Server Error' });
//     }
// });


router.post('/update/code', async (req, res) => {
    try {
        const code = await appSumoCode.findOneAndUpdate(
            { code: req.body.code, isUsed: false }, // Only update if the code is not already used
            {
                $set: {
                    usedBy: req.body.usedId,
                    isUsed: true,
                    dateUsed: new Date()
                }
            },
            { new: true }
        );

        if (!code) {
            return res.status(404).json({ message: 'Code not found or already used.' });
        }

        res.status(200).json({ message: 'Code successfully updated.' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error.' });
    }
});




module.exports = router
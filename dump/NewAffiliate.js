const express = require('express')
const router = express.Router()
const Afilliate = require('../Schema/Afilliate')
const PaymentAffiliate = require('../Schema/PaymentAffiliate')
const SubAffiliate = require('../Schema/SubAffiliate')
const LinkClicked = require("../Schema/LinkClicked")

const getUserRevenue = async (userId, date) => {
    try {

        let affiliateDataPayment
        if (date === undefined) {
            affiliateDataPayment = await PaymentAffiliate.findOne({ userId: userId }, { _id: 0, __v: 0, userId: 0 }).populate("affiliateUserAdded.transactionId")
        } else {
            affiliateDataPayment = await PaymentAffiliate.findOne({ userId: userId }, { _id: 0, __v: 0, userId: 0 })
                .populate({
                    path: "affiliateUserAdded.transactionId",
                    match: { createdAt: { $gte: date } },
                })
        }

        let revenue = 0
        affiliateDataPayment?.affiliateUserAdded.forEach(data => {
            if (data.transactionId !== null) {
                if (data.transactionId.paymentGateway === "stripe") {
                    if (data.transactionId.paymentInfo.currency === "inr") {
                        let inrTotal = data.transactionId.paymentInfo.amount_total / 100

                        const percentage = 15
                        const usdExchangeRate = 0.012
                        const percentageAmount = (inrTotal * percentage) / 100
                        revenue += percentageAmount * usdExchangeRate
                    } else {
                        let usdPrice = data.transactionId.paymentInfo.amount_total / 100
                        const percentage = 15
                        const percentageAmount = (usdPrice * percentage) / 100
                        revenue += percentageAmount
                    }
                } else if (data.transactionId.paymentGateway === "ccavenue") {
                    if (data.transactionId.paymentInfo.currency.trim() === "INR") {
                        let inrPrice = parseFloat(data.transactionId.paymentInfo.amount)

                        const percentage = 15
                        const usdExchangeRate = 0.012
                        const percentageAmount = (inrPrice * percentage) / 100
                        revenue += percentageAmount * usdExchangeRate
                    } else {
                        let usdPrice = parseFloat(data.transactionId.paymentInfo.amount)
                        const percentage = 15
                        const percentageAmount = (usdPrice * percentage) / 100
                        revenue += percentageAmount
                    }
                } else {
                    // we are not taking inr price because stripe does not support that
                    let usdPrice = parseFloat(data.transactionId.paymentInfo.purchase_units[0].payments.captures[0].amount.value)
                    const percentage = 15
                    const percentageAmount = (usdPrice * percentage) / 100
                    revenue += percentageAmount
                }
            }
        })

        return revenue
    } catch (error) {
        console.log(error)
        return 0
    }
}

const getSubAffiliateRevenue = async (userId, date, getCustomers) => {
    try {
        let affiliateDataPayment

        if (date === undefined) {
            affiliateDataPayment = await PaymentAffiliate.findOne({ userId: userId }, { _id: 0, __v: 0, userId: 0 }).populate("affiliateUserAdded.transactionId")
        } else {
            affiliateDataPayment = await PaymentAffiliate.findOne({ userId: userId }, { _id: 0, __v: 0, userId: 0 })
                .populate({
                    path: "affiliateUserAdded.transactionId",
                    match: { createdAt: { $gte: date } },
                })
        }

        let revenue = 0
        let customers = 0
        affiliateDataPayment?.affiliateUserAdded.forEach(data => {
            if (data.transactionId !== null) {
                customers++
                if (data.transactionId.paymentGateway === "stripe") {
                    if (data.transactionId.paymentInfo.currency === "inr") {
                        let inrTotal = data.transactionId.paymentInfo.amount_total / 100

                        const percentage = 15
                        const usdExchangeRate = 0.012
                        const percentageAmount = (inrTotal * percentage) / 100
                        revenue += percentageAmount * usdExchangeRate
                    } else {
                        let usdPrice = data.transactionId.paymentInfo.amount_total / 100
                        const percentage = 15
                        const percentageAmount = (usdPrice * percentage) / 100
                        revenue += percentageAmount
                    }
                } else if (data.transactionId.paymentGateway === "ccavenue") {
                    if (data.transactionId.paymentInfo.currency.trim() === "INR") {
                        let inrPrice = parseFloat(data.transactionId.paymentInfo.amount)

                        const percentage = 15
                        const usdExchangeRate = 0.012
                        const percentageAmount = (inrPrice * percentage) / 100
                        revenue += percentageAmount * usdExchangeRate
                    } else {
                        let usdPrice = parseFloat(data.transactionId.paymentInfo.amount)
                        const percentage = 15
                        const percentageAmount = (usdPrice * percentage) / 100
                        revenue += percentageAmount
                    }
                } else {
                    // we are not taking inr price because stripe does not support that
                    let usdPrice = parseFloat(data.transactionId.paymentInfo.purchase_units[0].payments.captures[0].amount.value)
                    const percentage = 15
                    const percentageAmount = (usdPrice * percentage) / 100
                    revenue += percentageAmount
                }
            }
        })

        const subAffiliateCut = 2
        const percentageAmount = (revenue * subAffiliateCut) / 100

        if (getCustomers) {
            return {
                percentageAmount,
                customers
            }
        } else {
            return percentageAmount
        }
    } catch (error) {
        console.log(error)
        return 0
    }
}

router.get("/:id", async (req, res) => {
    try {
        // check signup data from the old affiliate schema and filter the user id from it
        const afilliateData = await Afilliate.findOne({ userId: req.params.id }, { _id: 0, __v: 0, userId: 0 })
            .populate({ path: 'afilliateUserAdded', select: 'email lastName firstName' });

        const linkClickedData = await LinkClicked.findOne({ userId: req.params.id }, { _id: 0, __v: 0, userId: 0 });
        const affiliateDataPayment = await PaymentAffiliate.findOne({ userId: req.params.id }, { _id: 0, __v: 0, userId: 0 }).populate("affiliateUserAdded.transactionId")

        const revenue = await getUserRevenue(req.params.id)

        const subAffiliateData = await SubAffiliate.find({ userId: req.params.id })

        let subAffiliateRevenue = [0]
        const promises = subAffiliateData.map(async data => {
            const revenue = await getSubAffiliateRevenue(data.subAffiliate)
            return revenue
        })
        subAffiliateRevenue = await Promise.all(promises);

        let totalOfSubAffiliateRevenue = 0
        subAffiliateRevenue.forEach(data => {
            totalOfSubAffiliateRevenue += data
        })

        const salesCount = affiliateDataPayment?.affiliateUserAdded.filter(data => data.transactionId !== null)

        res.status(200).json({
            linkClicksCount: linkClickedData?.days?.length || 0,
            usersDetails: afilliateData?.afilliateUserAdded || [],
            salesCount: salesCount?.length || 0,
            revenue: revenue + totalOfSubAffiliateRevenue,
            afilliateData: afilliateData
        })
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: "Internal Server Error" });
    }
})

router.get("/referral/:id/:days", async (req, res) => {
    try {
        const days = parseInt(req.params.days);
        const startDate = new Date();
        startDate.setDate(startDate.getDate() - (days - 1));

        let dateArray = Array.from({ length: days }, (_, index) => {
            const currentDate = new Date(startDate);
            currentDate.setDate(currentDate.getDate() + index);

            return {
                date: currentDate,
                clicks: 0,
                signups: 0,
                customers: 0,
                revenue: 0
            };
        });

        const currentDate = new Date(startDate);
        currentDate.setHours(0, 0, 0, 0);

        // User sign ups
        const afilliateData = await Afilliate.findOne({ userId: req.params.id }, { _id: 0, __v: 0, userId: 0 })
            .populate({
                path: "afilliateUserAdded",
                match: { createdAt: { $gte: currentDate } },
                select: "_id, createdAt"
            });

        for (const data of dateArray) {
            if (afilliateData !== null) {
                for (const userDetails of afilliateData.afilliateUserAdded) {
                    const dateData = data.date.toDateString();
                    const userDetailsDate = userDetails.createdAt.toDateString();

                    if (dateData === userDetailsDate) {
                        data.signups++;
                    }
                }
            }
        }

        // Link clicks
        const linkClickedData = await LinkClicked.findOne({ userId: req.params.id, days: { $gte: currentDate } });
        if (linkClickedData !== null) {
            for (const data of dateArray) {
                for (const clickedDate of linkClickedData.days) {
                    if (data.date.toDateString() === clickedDate.toDateString()) {
                        data.clicks++;
                    }
                }
            }
        }

        // Customers and revenue
        const afilliateDataPayment = await PaymentAffiliate.findOne({ userId: req.params.id }, { _id: 0, __v: 0, userId: 0 })
            .populate({
                path: "affiliateUserAdded.transactionId",
                match: { createdAt: { $gte: currentDate } },
            });

        if (afilliateDataPayment !== null) {
            for (const data of dateArray) {
                for (const transaction of afilliateDataPayment.affiliateUserAdded) {
                    if (transaction.transactionId !== null) {
                        const dateData = data.date.toDateString();
                        const transactionDate = transaction.transactionId.createdAt.toDateString();

                        if (dateData === transactionDate) {
                            data.customers++;

                            if (transaction.transactionId.paymentGateway === "stripe") {
                                if (transaction.transactionId.paymentInfo.currency === "inr") {
                                    let inrTotal = transaction.transactionId.paymentInfo.amount_total / 100

                                    const percentage = 15
                                    const usdExchangeRate = 0.012
                                    const percentageAmount = (inrTotal * percentage) / 100
                                    data.revenue += percentageAmount * usdExchangeRate
                                } else {
                                    let usdPrice = transaction.transactionId.paymentInfo.amount_total / 100
                                    const percentage = 15
                                    const percentageAmount = (usdPrice * percentage) / 100
                                    data.revenue += percentageAmount
                                }
                            } else if (transaction.transactionId.paymentGateway === "ccavenue") {
                                if (transaction.transactionId.paymentInfo.currency.trim() === "INR") {
                                    let inrPrice = parseFloat(transaction.transactionId.paymentInfo.amount)

                                    const percentage = 15
                                    const usdExchangeRate = 0.012
                                    const percentageAmount = (inrPrice * percentage) / 100
                                    data.revenue += percentageAmount * usdExchangeRate
                                } else {
                                    let usdPrice = parseFloat(transaction.transactionId.paymentInfo.amount)
                                    const percentage = 15
                                    const percentageAmount = (usdPrice * percentage) / 100
                                    data.revenue += percentageAmount
                                }
                            } else {
                                // we are not taking inr price because stripe does not support that
                                let usdPrice = parseFloat(data.transactionId.paymentInfo.purchase_units[0].payments.captures[0].amount.value)
                                const percentage = 15
                                const percentageAmount = (usdPrice * percentage) / 100
                                data.revenue += percentageAmount
                            }
                        }
                    }
                }
            }
        }

        res.send(dateArray);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Internal Server Error" });
    }
});


router.get("/insight/:id/:days", async (req, res) => {
    try {
        const currentDate = new Date();
        currentDate.setDate(currentDate.getDate() - parseInt(req.params.days));
        currentDate.setHours(0, 0, 0, 0);

        // Customers and revenue
        const afilliateDataPayment = await PaymentAffiliate.findOne({ userId: req.params.id }, { _id: 0, __v: 0, userId: 0 })
            .populate({
                path: "affiliateUserAdded.transactionId",
                match: { createdAt: { $gte: currentDate } },
            })
            .populate({
                path: "affiliateUserAdded.userId",
            });

        let userInsights = []
        userInsights = afilliateDataPayment?.affiliateUserAdded.map(data => {
            let revenue = 0

            if (data.transactionId === null) return null
            if (data.transactionId.paymentGateway === "stripe") {
                if (data.transactionId.paymentInfo.currency === "inr") {
                    let inrTotal = data.transactionId.paymentInfo.amount_total / 100

                    const percentage = 15
                    const usdExchangeRate = 0.012
                    const percentageAmount = (inrTotal * percentage) / 100
                    revenue += percentageAmount * usdExchangeRate
                } else {
                    let usdPrice = data.transactionId.paymentInfo.amount_total / 100
                    const percentage = 15
                    const percentageAmount = (usdPrice * percentage) / 100
                    revenue += percentageAmount
                }
            } else if (data.transactionId.paymentGateway === "ccavenue") {
                if (data.transactionId.paymentInfo.currency.trim() === "INR") {
                    let inrPrice = parseFloat(data.transactionId.paymentInfo.amount)

                    const percentage = 15
                    const usdExchangeRate = 0.012
                    const percentageAmount = (inrPrice * percentage) / 100
                    revenue += percentageAmount * usdExchangeRate
                } else {
                    let usdPrice = parseFloat(data.transactionId.paymentInfo.amount)
                    const percentage = 15
                    const percentageAmount = (usdPrice * percentage) / 100
                    revenue += percentageAmount
                }
            } else {
                // we are not taking inr price because stripe does not support that
                let usdPrice = parseFloat(data.transactionId.paymentInfo.purchase_units[0].payments.captures[0].amount.value)
                const percentage = 15
                const percentageAmount = (usdPrice * percentage) / 100
                revenue += percentageAmount
            }

            return {
                name: data.userId.firstName + " " + data.userId.lastName,
                dateJoined: data.userId.createdAt,
                purchasedDate: data.transactionId.createdAt,
                planName: "",
                status: "active",
                revenue: revenue
            }
        })

        userInsights = userInsights || []
        if (userInsights.length > 1) {
            userInsights = userInsights.filter(data => data !== null)
        }
        res.send(userInsights)
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Internal Server Error" });
    }
})

router.get("/payout/:id", async (req, res) => {
    try {
        const firstDayOfMonth = new Date(); // Creates a new Date object with the current date and time
        firstDayOfMonth.setDate(1); // Sets the day of the month to 1
        firstDayOfMonth.setHours(0, 0, 0, 0);

        const affiliateDataPayment = await PaymentAffiliate.findOne({ userId: req.params.id }, { _id: 0, __v: 0, userId: 0 })
            .populate({
                path: "affiliateUserAdded.transactionId",
                match: { createdAt: { $gte: firstDayOfMonth } },
            })

        const revenue = await getUserRevenue(req.params.id, firstDayOfMonth)

        const subAffiliateData = await SubAffiliate.find({ userId: req.params.id })
        let subAffiliateRevenue = [0]
        const promises = subAffiliateData.map(async data => {
            const revenue = await getSubAffiliateRevenue(data.subAffiliate, firstDayOfMonth)
            return revenue
        })
        subAffiliateRevenue = await Promise.all(promises);

        let totalOfSubAffiliateRevenue = 0
        subAffiliateRevenue.forEach(data => {
            totalOfSubAffiliateRevenue += data
        })

        const currentDate = new Date(); // Creates a new Date object with the current date and time
        currentDate.setDate(1); // Sets the day of the month to 1
        currentDate.setHours(0, 0, 0, 0);
        currentDate.setDate(currentDate.getDate() + 50);

        const salesCount = affiliateDataPayment?.affiliateUserAdded.filter(data => data.transactionId !== null)

        res.status(200).json({
            startDate: firstDayOfMonth,
            payoutDate: currentDate,
            salesCount: salesCount?.length || 0,
            revenue: revenue + totalOfSubAffiliateRevenue
        });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Internal Server Error" });
    }
})


router.get("/sub/:id/:days", async (req, res) => {
    try {
        const currentDate = new Date();
        currentDate.setDate(currentDate.getDate() - parseInt(req.params.days));
        currentDate.setHours(0, 0, 0, 0);

        const subAffiliateData = await SubAffiliate.find({ userId: req.params.id }, { _id: 0, updatedAt: 0 }).populate({
            path: "subAffiliate",
            select: "firstName lastName"
        }).lean()


        const promises = subAffiliateData.map(async data => {
            const getCustomers = true
            const { percentageAmount, customers } = await getSubAffiliateRevenue(data.subAffiliate, currentDate, getCustomers)
            return {
                ...data,
                revenue: percentageAmount,
                customers
            }
        })

        let subAffiliateWithRevenue = await Promise.all(promises);
        res.send(subAffiliateWithRevenue)
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Internal Server Error" });
    }
})

module.exports = router
const updatePassword = async (req, res) => {
    try {
        const data = await user.findOne({ email: req.body.email });
        if (data === null) {
            return res.status(404).json({
                message: 'user not found',
            });
        } else {
            //compare the old password and current password
            const result = await bcrypt.compare(req.body.oldpassword, data.password);
            if (!result) {
                return res.status(400).json({
                    message: 'password not match',
                });
            } else {
                data.failedLoginAttempts = 0;
                data.password = await bcrypt.hash(req.body.newpassword, 10);
                data.save();
                return res.status(200).json({
                    message: 'updated successfully',
                });
            }
        }
    } catch (error) {
        logger.error(error);
        return res.status(500).json({
            error: error,
        });
    }
};


const getUserDetails = async (req, res) => {
    try {
        if (req.body !== null) {
            const start = new Date(req.body.currentDate);
            start.setHours(0, 0, 0, 0);
            const end = new Date(req.body.currentDate);
            end.setHours(23, 59, 59, 999);
            const paidUser = await UserFeatureFactory.find({ planId: req.body.startup_plan_id });
            const freeUser = await UserFeatureFactory.find({ planId: req.body.free_plan_id });
            const creatorUsers = await UserFeatureFactory.find({ planId: req.body.cretor_plan_id });
            const businessUsers = await UserFeatureFactory.find({ planId: req.body.business_plan_id });

            const currentUser = await User.find({ createdAt: { $gte: start, $lt: end } });

            return res.status(201).json({
                status: 201,
                paidUser: paidUser.length,
                freeUser: freeUser.length,
                creatorUsers: creatorUsers.length,
                businessUsers: businessUsers.length,
                currentUser: currentUser.length,
                message: 'user data fetch successfully...'
            });

        }
    } catch (error) {
        logger.error(error);
        return res.status(500).json({
            error: error,
        });
    }
};

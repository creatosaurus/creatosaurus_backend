const express = require('express')
const router = express.Router()
const onBoarding = require('../Schema/Onboarding')
const logger = require("../logger")

router.post("/", async (req, res) => {
    try {
        const user = await onBoarding.findOne({ userId: req.body.userId })
        await onBoarding.create(req.body)
        res.status(201).json({
            message: 'created'
        })
    } catch (error) {
        logger.error(error)
        res.status(201).json({
            error: error
        })
    }
})

router.get("/:id", async (req, res) => {
    try {
        const user = await onBoarding.findOne({ userId: req.params.id })
        res.send(user)
    } catch (error) {
        logger.error(error)
        res.status(201).json({
            error: error
        })
    }
})

module.exports = router
const express = require('express')
const user = require('../Schema/User')
const router = express.Router()
const logger = require("../logger")

router.get('/', async (req, res) => {
    try {
        const userData = await user.find({}, { password: 0 })
        res.send(userData)
    } catch (error) {
        logger.error(error)
    }
})

router.post('/:id', async (req, res) => {
    try {
        const userData = await user.findById(req.params.id)
        userData.isAdmin = !userData.isAdmin
        await userData.save()
        res.send('done')
    } catch (error) {
        logger.error(error)
    }
})

module.exports = router
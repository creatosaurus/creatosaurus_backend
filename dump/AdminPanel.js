const express = require('express')
const User = require('../Schema/User')
const router = express.Router()

router.get('/', async (req, res) => {
    try {
        // Get the page number from the query parameters
        const page = parseInt(req.query.page) || 1;

        // Get the page size (number of items per page) from the query parameters
        const pageSize = parseInt(req.query.limit) || 10;

        const skip = (page - 1) * pageSize;

        // Use the aggregation pipeline to skip and limit the results for pagination
        const userData = await User.aggregate([
            { $project: { email: 1, firstName: 1, lastName: 1, emailVerified: 1, createdAt: 1, onboarding_status: 1, isAdmin: 1, isAffiliate: 1, freeTrial: 1 } }, // Exclude the 'password' field
            { $sort: { _id: -1 } }, // Replace 'fieldNameToSortBy' with the actual field you want to sort by
            { $skip: skip },
            { $limit: pageSize }
        ]);

        res.send(userData)
    } catch (error) {
        console.log(error)
    }
})

router.get('/search', async (req, res) => {
    try {
        // Get the page number from the query parameters
        const page = parseInt(req.query.page) || 1;

        // Get the page size (number of items per page) from the query parameters
        const pageSize = parseInt(req.query.limit) || 10;

        const skip = (page - 1) * pageSize;

        // Use the aggregation pipeline to skip and limit the results for pagination
        const results = await User.find({
            $or: [
                { email: { $regex: req.query.query, $options: 'i' } }, // Case-insensitive email search
                { firstName: { $regex: req.query.query, $options: 'i' } }, // Case-insensitive first name search
                { lastName: { $regex: req.query.query, $options: 'i' } }, // Case-insensitive last name search
            ],
        }, { email: 1, firstName: 1, lastName: 1, emailVerified: 1, createdAt: 1, onboarding_status: 1, isAdmin: 1, isAffiliate: 1, freeTrial: 1 }).skip(skip).limit(pageSize)

        res.send(results)
    } catch (error) {
        console.log(error)
    }
})

module.exports = router
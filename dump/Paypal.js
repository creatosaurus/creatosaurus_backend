const express = require("express")
const router = express.Router()
const user = require("../Schema/User")
const CryptoJS = require('crypto-js');
const workspace = require('../Schema/Workspace')
const plan = require('../Schema/Plan')
const featureFactory = require('../Schema/UserFeatureFactory')
const transaction = null
const manageCacheAccounts = require('../Functions/ManageCacheAccounts')
const Affiliate = require("../Schema/PaymentAffiliate")
const { sendEmail } = require('../Functions/paymentConfirmEmail')
const objectId = require('mongoose').Types.ObjectId;
const axios = require('axios')



const BASE_URL = 'https://api-m.paypal.com';
const CLIENT_ID = 'Adfxbh1lifScdMC-AUUSj4zgcderQ2tRVBnx1HRAiw4dsCcJjEpzJNErb39VB9BVmdpmZ1GXlHVyugWT';
const CLIENT_SECRET = 'EIdC1SDDk6YnIeMH1OHQPtMmEqL3Ud_lfqeT2JtnViyr3HzGzmv-y9L-5qzkRX6tk7ONlStzAQ-ezGjz';

const generateAccessToken = async () => {
    try {
        if (!CLIENT_ID || !CLIENT_SECRET) {
            throw new Error("MISSING_API_CREDENTIALS");
        }
        const auth = Buffer.from(`${CLIENT_ID}:${CLIENT_SECRET}`).toString("base64");

        const response = await axios.post(`${BASE_URL}/v1/oauth2/token`, 'grant_type=client_credentials', {
            headers: {
                Authorization: `Basic ${auth}`,
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        });

        const data = response.data;
        return data.access_token;
    } catch (error) {
        console.error("Failed to generate Access Token:", error);
        throw error; // Re-throw the error for consistent error handling
    }
};

const generateClientToken = async () => {
    const accessToken = await generateAccessToken();
    const url = `${BASE_URL}/v1/identity/generate-token`;
    const response = await axios.post(url, {}, {
        headers: {
            Authorization: `Bearer ${accessToken}`,
            "Accept-Language": "en_US",
            "Content-Type": "application/json",
        }
    })

    return response.data;
};


router.post("/client/token", async (req, res) => {
    try {
        const response = await generateClientToken()
        res.status(200).json({
            token: response.client_token,
            expires_in: response.expires_in
        })
    } catch (error) {
        console.error("Failed to generate client token:", error);
        res.status(500).send({ error: "Failed to generate client token." });
    }
});

router.post("/create/order", async (req, res) => {
    try {
        let userData = await user.findOne({ _id: req.body.userId })
        if (userData === null) return res.status(400).json({ error: "user not found" })
        const { amount, currency } = req.body;

        const computedHashAmount = CryptoJS.SHA256(req.body.amount).toString()
        const computedHashPlanId = CryptoJS.SHA256(req.body.planId).toString()

        if (computedHashAmount !== req.body.hasm) return res.status(400).json({ error: "Invalid Request" })
        if (computedHashPlanId !== req.body.hpsl) return res.status(400).json({ error: "Invalid Request" })

        userData.userDetails = req.body.userDetails
        await userData.save()

        const accessToken = await generateAccessToken();

        const payload = {
            intent: "CAPTURE",
            purchase_units: [
                {
                    amount: {
                        currency_code: currency,
                        value: amount,
                    }
                },
            ],
        };

        const response = await axios.post(`${BASE_URL}/v2/checkout/orders`, JSON.stringify(payload), {
            headers: {
                Authorization: `Bearer ${accessToken}`,
                "Accept-Language": "en_US",
                "Content-Type": "application/json",
            }
        })

        return res.status(200).json({
            order: response.data
        })


    } catch (error) {
        console.error("Failed to create order:", error);
        res.status(500).json({ error: "Failed to create order." });
    }
});


router.post("/order/:orderId/capture", async (req, res) => {
    try {
        const { orderId } = req.params;
        const accessToken = await generateAccessToken();
        const response = await axios.post(`${BASE_URL}/v2/checkout/orders/${orderId}/capture`, {}, {
            headers: {
                Authorization: `Bearer ${accessToken}`,
                "Content-Type": "application/json",
            }
        })

        return res.status(200).json({
            order: response.data
        })

    } catch (error) {
        res.status(500).json({ error: "Failed to capture order." });
    }
});


router.post("/upgrade/plan", async (req, res) => {
    try {
        const paymentData = req.body;
        const obj = {
            userId: paymentData.userId,
            paymentGateway: "paypal",
            planInfo: {
                planId: paymentData.planId,
                lifeTimeDeal: false,
                originalPriceOfPlan: paymentData.originalPriceOfPlan,
                tax: paymentData.tax,
                couponCode: paymentData.couponCode
            },
            paymentInfo: paymentData.payment
        }

        const transactionData = await transaction.create(obj)
        const affiliateId = paymentData.invitationCode
        const userId = paymentData.userId

        if (affiliateId != null && objectId.isValid(affiliateId)) {
            const userData = await user.findById(affiliateId)  // check the referal id of user is affiliate or not

            if (userData && userData.isAffiliate) {
                // check user current plan if free then only add to affiliate program
                const featureFactoryData = await featureFactory.findOne({ userId: userId }, { planId: 1 })
                const freePlanId = "6247087d37b553074fb6b25c"

                if (featureFactoryData.planId.toString() === freePlanId) {
                    // check affiliate user is there or not
                    const affiliateData = await Affiliate.findOne({ userId: affiliateId })

                    const userToAddData = {
                        userId: userId,
                        transactionId: transactionData._id
                    }

                    if (affiliateData !== null) {
                        affiliateData.affiliateUserAdded = [...affiliateData.affiliateUserAdded, userToAddData]
                        await affiliateData.save()
                    } else {
                        await Affiliate.create({
                            userId: affiliateId,
                            affiliateUserAdded: [userToAddData]
                        })
                    }
                }
            }
        }

        const workspaceData = await workspace.find({ user_id: userId });
        const planData = await plan.findById(paymentData.planId);

        // Update workspace data
        if (workspaceData) {
            const promises = workspaceData?.map(async (workspace) => {
                // Set team user state inactive
                workspace.team = workspace.team.map((member) => {
                    member.active = workspace.user_email === member.user_email;
                    return member;
                });

                // Make older workspace visible
                if (!workspace.active) {
                    workspace.active = true;
                }

                return workspace.save();
            });

            await Promise.all(promises);
        }

        // Update feature factory data
        const featureFactoryData = await featureFactory.findOne({ userId: paymentData.userId });

        featureFactoryData.planId = planData._id;
        featureFactoryData.workspace = planData.workspace;
        featureFactoryData.users = planData.users;
        featureFactoryData.socialAccounts = planData.socialAccounts;
        featureFactoryData.schedulePost = planData.schedulePost;

        // Handle AI credits
        featureFactoryData.aiCredites = planData.aiCredites
        featureFactoryData.hashtagSearch = planData.hashtagSearch;
        featureFactoryData.hashtagSearchInstagram = planData.hashtagSearchInstagram;
        featureFactoryData.multipleTemplateResolution = planData.multipleTemplateResolution;
        featureFactoryData.transperentBackground = planData.transperentBackground
        featureFactoryData.planTakenForTime = paymentData.subscriptionType;

        // Set reCreditDate and endDate based on subscriptionType and lifeTimeDeal
        featureFactoryData.reCreditDate = paymentData.subscriptionType === "monthly" ? null : Date.now() + 30 * 24 * 60 * 60 * 1000;
        featureFactoryData.endDate = false
            ? Date.now() + 36500 * 24 * 60 * 60 * 1000
            : paymentData.subscriptionType === "monthly"
                ? Date.now() + 30 * 24 * 60 * 60 * 1000
                : Date.now() + 365 * 24 * 60 * 60 * 1000;

        featureFactoryData.startDate = Date.now();
        await featureFactoryData.save();

        manageCacheAccounts(paymentData.userId);

        // NOTE: get user details and send a payment confirmation mail
        const userData = await user.findOne({ _id: paymentData.userId })
        sendEmail(userData.email, userData.userDetails.firstName, planData.planName, paymentData.payment.purchase_units[0].payments.captures[0].amount.currency_code + " " + paymentData.payment.purchase_units[0].payments.captures[0].amount.value, "paypal")
        res.status(200).json({
            message: "success"
        })
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: "Internal Server Error"
        })
    }
})




module.exports = router
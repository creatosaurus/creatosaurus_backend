const express = require('express')
const router = express.Router()
const featureFactory = require('../Schema/UserFeatureFactory')
const couponCode = require("../Schema/CouponCode")
const workspace = require('../Schema/Workspace')
const plan = require('../Schema/Plan')
const user = require("../Schema/User")
const checkAuth = require('../Middleware/CheckAuth')
const transaction = null
const Affiliate = require("../Schema/PaymentAffiliate")
const manageCacheAccounts = require('../Functions/ManageCacheAccounts')
const objectId = require('mongoose').Types.ObjectId;
const { sendEmail } = require('../Functions/paymentConfirmEmail')
const CheckAuth = require('../Middleware/CheckAuth')

const stripe = require("stripe")("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", {
  apiVersion: "2022-08-01",
});


const STRIPE_WEBHOOK_SECRET = "whsec_d2ppM5WmBGnwRJIOfYqwWMMnY9gFaJws"; //live
//const STRIPE_WEBHOOK_SECRET = "whsec_FS0HquyaZnlZroywQT5mkgwrH8CQacXj"; //test
router.post("/stripe/webhook", express.raw({ type: 'application/json' }), async (request, response) => {
  try {
    const sig = request.headers['stripe-signature'];
    const event = stripe.webhooks.constructEvent(request.body, sig, STRIPE_WEBHOOK_SECRET);

    // Handle the event
    if (event.type === "checkout.session.completed") {
      // Parse payment object from event data
      const paymentObject = event.data.object.metadata

      // Create transaction object
      const obj = {
        userId: paymentObject.userId,
        paymentGateway: "stripe",
        planInfo: {
          planId: paymentObject.planId,
          users: paymentObject.users,
          lifeTimeDeal: paymentObject.lifeTimeDeal,
          originalPriceOfPlan: paymentObject.originalPriceOfPlan,
          tax: paymentObject.tax,
          couponCode: {
            code: paymentObject.couponCodeName,
            discount: paymentObject.code
          }
        },
        paymentInfo: event.data.object
      }

      // Create transaction record
      const transactionData = await transaction.create(obj)

      const affiliateId = paymentObject.invitationCode
      const userId = paymentObject.userId

      if (affiliateId != null && objectId.isValid(affiliateId)) {
        const userData = await user.findById(affiliateId)  // check the referal id of user is affiliate or not

        if (userData && userData.isAffiliate) {
          // check user current plan if free then only add to affiliate program
          const featureFactoryData = await featureFactory.findOne({ userId: userId }, { planId: 1 })
          const freePlanId = "6247087d37b553074fb6b25c"

          if (featureFactoryData.planId.toString() === freePlanId) {
            // check affiliate user is there or not
            const affiliateData = await Affiliate.findOne({ userId: affiliateId })

            const userToAddData = {
              userId: userId,
              transactionId: transactionData._id
            }

            if (affiliateData !== null) {
              affiliateData.affiliateUserAdded = [...affiliateData.affiliateUserAdded, userToAddData]
              await affiliateData.save()
            } else {
              await Affiliate.create({
                userId: affiliateId,
                affiliateUserAdded: [userToAddData]
              })
            }
          }
        }
      }

      // Find workspace and plan data
      const workspaceData = await workspace.find({ user_id: paymentObject.userId });
      const planData = await plan.findById(paymentObject.planId);

      // Update workspace data
      if (workspaceData) {
        const promises = workspaceData?.map(async (workspace) => {
          // Set team user state inactive
          workspace.team = workspace.team.map((member) => {
            member.active = workspace.user_email === member.user_email;
            return member;
          });

          // Make older workspace visible
          if (!workspace.active) {
            workspace.active = true;
          }

          return workspace.save();
        });

        await Promise.all(promises);
      }

      // Update feature factory data
      const featureFactoryData = await featureFactory.findOne({ userId: paymentObject.userId });

      featureFactoryData.planId = planData._id;
      featureFactoryData.workspace = planData.workspace;
      featureFactoryData.users = planData.users;
      featureFactoryData.socialAccounts = planData.socialAccounts;
      featureFactoryData.schedulePost = planData.schedulePost;

      // Handle AI credits
      featureFactoryData.aiCredites = planData.aiCredites
      featureFactoryData.hashtagSearch = planData.hashtagSearch;
      featureFactoryData.hashtagSearchInstagram = planData.hashtagSearchInstagram;
      featureFactoryData.multipleTemplateResolution = planData.multipleTemplateResolution;
      featureFactoryData.transperentBackground = planData.transperentBackground
      featureFactoryData.planTakenForTime = paymentObject.subscriptionType;

      // Set reCreditDate and endDate based on subscriptionType and lifeTimeDeal
      featureFactoryData.reCreditDate = paymentObject.subscriptionType === "monthly" ? null : Date.now() + 30 * 24 * 60 * 60 * 1000;
      featureFactoryData.endDate = paymentObject.lifeTimeDeal
        ? Date.now() + 36500 * 24 * 60 * 60 * 1000
        : paymentObject.subscriptionType === "monthly"
          ? Date.now() + 30 * 24 * 60 * 60 * 1000
          : Date.now() + 365 * 24 * 60 * 60 * 1000;

      featureFactoryData.startDate = Date.now();
      await featureFactoryData.save();


      // NOTE: get user details and send a payment confirmation mail
      const userData = await user.findOne({ _id: paymentObject.userId })
      sendEmail(userData.email, userData.userDetails.firstName, planData.planName, event.data.object.currency + " " + event.data.object.amount_total, event.data.object.payment_method_types[0])
      manageCacheAccounts(paymentObject.userId);
    }

    // Return a 200 response to acknowledge receipt of the event
    response.send();
  } catch (error) {
    console.log(error)
    response.status(400).send(`Webhook Error: ${error.message}`);
  }
})



router.post('/coupon/code/plan', express.json(), checkAuth, async (req, res) => {
  try {
    // make old workspace active
    const workspaceData = await workspace.find({ user_id: req.body.userId })

    // check for app sumo deal
    if (req.body.planName === "captionator") {
      let couponCodeData = await couponCode.findOne({ code: req.body?.code })
      couponCodeData.used = true
      await couponCodeData.save()

      let featureFactoryData = await featureFactory.findOne({ userId: req.body.userId })
      featureFactoryData.planId = req.body.planId
      featureFactoryData.workspace = "1"
      featureFactoryData.users = "3"
      featureFactoryData.socialAccounts = "3"
      featureFactoryData.schedulePost = "10"
      featureFactoryData.aiCredites = "50000"
      featureFactoryData.hashtagSearch = "0"
      featureFactoryData.hashtagSearchInstagram = "5"
      featureFactoryData.multipleTemplateResolution = false
      featureFactoryData.transperentBackground = false
      featureFactoryData.planTakenForTime = "yearly"
      featureFactoryData.couponCode = req.body?.code
      featureFactoryData.userPlans = ["captionator"]
      featureFactoryData.deal = "app sumo"

      let date = new Date()
      let date2 = new Date()
      featureFactoryData.reCreditDate = date.setDate(date.getDate() + 30)
      featureFactoryData.endDate = date2.setDate(date2.getDate() + 73000)
      featureFactoryData.startDate = date
      await featureFactoryData.save()
      res.send("done")
    } else {
      const planData = await plan.findById("646cd137782a707fb31903b4")

      for await (const data of workspaceData) {
        // make teams user state inactive
        data.team = data.team.map(data1 => {
          if (data.user_email === data1.user_email) {
            data1.active = true
          } else {
            data1.active = false
          }
          return data1
        })

        // make older workspace visible
        if (data.active === false) {
          data.active = true
        }

        await data.save()
      }


      let featureFactoryData = await featureFactory.findOne({ userId: req.body.userId })
      featureFactoryData.planId = planData._id
      featureFactoryData.workspace = planData.workspace
      featureFactoryData.users = planData.users
      featureFactoryData.socialAccounts = planData.socialAccounts
      featureFactoryData.schedulePost = planData.schedulePost
      featureFactoryData.aiCredites = planData.aiCredites
      featureFactoryData.hashtagSearch = planData.hashtagSearch
      featureFactoryData.hashtagSearchInstagram = planData.hashtagSearchInstagram
      featureFactoryData.multipleTemplateResolution = planData.multipleTemplateResolution
      featureFactoryData.transperentBackground = planData.transperentBackground
      featureFactoryData.planTakenForTime = `${parseInt(req.body.days)} days`
      featureFactoryData.couponCode = req.body?.code

      let date = new Date()
      let date2 = new Date()
      featureFactoryData.reCreditDate = date.setDate(date.getDate() + 30)
      featureFactoryData.endDate = date2.setDate(date2.getDate() + parseInt(req.body.days))
      featureFactoryData.startDate = date
      await featureFactoryData.save()
      res.send("done")
    }
  } catch (error) {
    res.status(500).json({
      error: error
    })
  }
})


router.get("/details", express.json(), CheckAuth, async (req, res) => {
  try {
    const transactionDetails = await transaction.find({ userId: req.body.userId }).sort({ '_id': -1 })
    res.send(transactionDetails)
  } catch (error) {
    res.status(500).json({
      error: "Internal server error"
    })
  }
})

router.post("/subscription/details", express.json(), async (req, res) => {
  try {
    const customer = await stripe.customers.retrieve(req.body.id);
    res.send(customer)
  } catch (error) {
    console.log(error)
    res.status(500).json({
      error: "Internal server error"
    })
  }
})

router.post("/subscription/pause", express.json(), async (req, res) => {
  try {
    const subscription = await stripe.subscriptions.update(req.body.id, {
      pause_collection: {
        behavior: 'void'
      },
    });

    res.send(subscription)
  } catch (error) {
    console.log(error)
    res.status(500).json({
      error: "Internal server error"
    })
  }
})


router.post("/subscription/resume", express.json(), async (req, res) => {
  try {
    const subscription = await stripe.subscriptions.update(req.body.id, {
      pause_collection: ''
    });

    res.send(subscription)
  } catch (error) {
    console.log(error)
    res.status(500).json({
      error: "Internal server error"
    })
  }
})

router.post("/subscription/delete", express.json(), async (req, res) => {
  try {
    const subscription = await stripe.subscriptions.del(req.body.id);
    res.send(subscription)
  } catch (error) {
    console.log(error)
    res.status(500).json({
      error: "Internal server error"
    })
  }
})

router.put("/subscription/update", express.json(), async (req, res) => {
  try {
    // Find user's subscription data in database
    const userData = await transaction.findOne({ userId: req.body.userId })
    if (!userData) {
      return res.status(404).json({
        error: 'User subscription not found'
      })
    }

    // Update user's subscription using Stripe API
    const subscription = await stripe.subscriptions.update(userData.paymentInfo.subscription, {
      items: [
        { plan: req.body.price, quantity: req.body.quantity }
      ],
      cancel_at_period_end: true
    })

    // Send success response
    res.send('Subscription updated successfully')
  } catch (error) {
    console.log(error)
    res.status(500).json({
      error: "Internal server error"
    })
  }
})


router.post("/create-checkout-session", express.json(), async (req, res) => {
  try {
    let userData = await user.findOne({ _id: req.body.userId })
    if (userData === null) return res.status(400).json({ error: "user not found" })

    const session = await stripe.checkout.sessions.create({
      payment_method_types: ['card'],
      billing_address_collection: 'auto',
      line_items: [
        {
          price: req.body.price,
          quantity: req.body.quantity,
        },
      ],
      mode: 'subscription',
      customer_email: userData.email,
      allow_promotion_codes: true,
      metadata: {
        userId: req.body.userId,
        subscriptionType: req.body.subscriptionType,
        planId: req.body.planId,
        users: req.body.users,
        lifeTimeDeal: req.body.lifeTimeDeal
      },
      client_reference_id: req.body.userId,
      success_url: `https://www.app.creatosaurus.io/confirmation`,
      cancel_url: `https://www.app.creatosaurus.io/?canceled=true`,
    });

    res.send(session.url)
  } catch (error) {
    res.status(500).json({ error: "Internal server error" })
    console.log(error)
  }
})


module.exports = router
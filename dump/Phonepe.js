const express = require("express")
const router = express.Router()
const user = require("../Schema/User")
const CryptoJS = require('crypto-js');
const workspace = require('../Schema/Workspace')
const plan = require('../Schema/Plan')
const featureFactory = require('../Schema/UserFeatureFactory')
const transaction = null
const manageCacheAccounts = require('../Functions/ManageCacheAccounts')
const Affiliate = require("../Schema/PaymentAffiliate")
const { sendEmail } = require('../Functions/paymentConfirmEmail');
const objectId = require('mongoose').Types.ObjectId;
const axios = require('axios')
const crypto = require('crypto');


router.post('/checkout', async (req, res) => {
    try {
        let userData = await user.findOne({ _id: req.body.userId })
        if (userData === null) return res.status(400).json({ error: "user not found" })


        const computedHashAmount = CryptoJS.SHA256(req.body.amount).toString()
        const computedHashPlanId = CryptoJS.SHA256(req.body.planId).toString()

        if (computedHashAmount !== req.body.hasm) return res.status(400).json({ error: "Invalid Request" })
        if (computedHashPlanId !== req.body.hpsl) return res.status(400).json({ error: "Invalid Request" })

        userData.userDetails = req.body.userDetails
        await userData.save()


        const transactionId = req.body.order_id
        const userId = req.body.userId

        const merchantId = 'M14HREFX602I'
        const saltKey = '579528ea-7cdc-46f6-8aa6-ed245b2a18bd'

        const paymentBody = {
            "merchantId": merchantId,
            "merchantTransactionId": transactionId,
            "merchantUserId": `MUID${userId}`,
            "amount": req.body.amount * 100,
            "redirectUrl": `https://api.app.creatosaurus.io/creatosaurus/phonepe/transaction/complete/${transactionId}?plan=${req.body.planId}&user=${req.body.userId}&type=${req.body.subscriptionType}&originalPlanPrice=${req.body.originalPriceOfPlan}&tax=${req.body.tax}&couponCode=${req.body.couponCode}&invitationCode=${req.body.invitationCode}`,
            "redirectMode": "POST",
            "callbackUrl": "https://api.app.creatosaurus.io/creatosaurus/phonepe/callback",
            "mobileNumber": req.body.userDetails.phoneNumber,
            "paymentInstrument": {
                "type": "PAY_PAGE"
            }
        }


        const payload = JSON.stringify(paymentBody);
        const base64PaymentBody = Buffer.from(payload).toString('base64');
        const keyIndex = 1;
        const string = base64PaymentBody + '/pg/v1/pay' + saltKey;
        const sha256 = crypto.createHash('sha256').update(string).digest('hex');
        const checksum = sha256 + '###' + keyIndex;

        const options = {
            headers: { accept: 'application/json', 'Content-Type': 'application/json', 'X-VERIFY': checksum }
        };

        const response = await axios.post('https://api.phonepe.com/apis/hermes/pg/v1/pay', {
            request: base64PaymentBody
        }, options)

        res.send(response.data.data.instrumentResponse.redirectInfo.url)
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: "Internal Server Error!"
        })
    }
})


router.post('/transaction/complete/:transactionId', async (req, res) => {
    try {
        const transactionId = req.params.transactionId
        const merchantId = 'M14HREFX602I'
        const saltKey = '579528ea-7cdc-46f6-8aa6-ed245b2a18bd'
        const keyIndex = 1;
        const dataString = `/pg/v1/status/${merchantId}/${transactionId}` + saltKey;
        const sha256 = crypto.createHash('sha256').update(dataString).digest('hex');
        const checksum = sha256 + "###" + keyIndex;
        const options = {
            headers: { accept: 'application/json', 'Content-Type': 'application/json', 'X-VERIFY': checksum, 'X-MERCHANT-ID': merchantId }
        };
        const response = await axios.get(`https://api.phonepe.com/apis/hermes/pg/v1/status/${merchantId}/${transactionId}`, options)

        const paymentInfo = response.data

        // NOTE: If payment fails redirect to cancel page
        if (!paymentInfo.success || paymentInfo.code === "PAYMENT_PENDING") return res.redirect('https://www.app.creatosaurus.io/?canceled=true');

        const obj = {
            userId: req.query.user,
            paymentGateway: "phonepe",
            planInfo: {
                planId: req.query.plan,
                lifeTimeDeal: false,
                originalPriceOfPlan: req.query.originalPlanPrice,
                tax: req.query.tax,
                couponCode: req.query.couponCode
            },
            paymentInfo: paymentInfo
        }

        const transactionData = await transaction.create(obj)


        const affiliateId = req.query.invitationCode
        const userId = req.query.user

        if (affiliateId != null && objectId.isValid(affiliateId)) {
            const userData = await user.findById(affiliateId)  // check the referal id of user is affiliate or not

            if (userData && userData.isAffiliate) {
                // check user current plan if free then only add to affiliate program
                const featureFactoryData = await featureFactory.findOne({ userId: userId }, { planId: 1 })
                const freePlanId = "6247087d37b553074fb6b25c"

                if (featureFactoryData.planId.toString() === freePlanId) {
                    // check affiliate user is there or not
                    const affiliateData = await Affiliate.findOne({ userId: affiliateId })

                    const userToAddData = {
                        userId: userId,
                        transactionId: transactionData._id
                    }

                    if (affiliateData !== null) {
                        affiliateData.affiliateUserAdded = [...affiliateData.affiliateUserAdded, userToAddData]
                        await affiliateData.save()
                    } else {
                        await Affiliate.create({
                            userId: affiliateId,
                            affiliateUserAdded: [userToAddData]
                        })
                    }
                }
            }
        }
        const workspaceData = await workspace.find({ user_id: req.query.user });
        const planData = await plan.findById(req.query.plan);

        // Update workspace data
        const promises = workspaceData.map(async (workspace) => {
            // Set team user state inactive
            workspace.team = workspace.team.map((member) => {
                member.active = workspace.user_email === member.user_email;
                return member;
            });

            // Make older workspace visible
            if (!workspace.active) {
                workspace.active = true;
            }

            return workspace.save();
        });

        await Promise.all(promises);

        // Update feature factory data
        const featureFactoryData = await featureFactory.findOne({ userId: req.query.user });

        featureFactoryData.planId = planData._id;
        featureFactoryData.workspace = planData.workspace;
        featureFactoryData.users = planData.users;
        featureFactoryData.socialAccounts = planData.socialAccounts;
        featureFactoryData.schedulePost = planData.schedulePost;

        // Handle AI credits
        featureFactoryData.aiCredites = planData.aiCredites
        featureFactoryData.hashtagSearch = planData.hashtagSearch;
        featureFactoryData.hashtagSearchInstagram = planData.hashtagSearchInstagram;
        featureFactoryData.multipleTemplateResolution = planData.multipleTemplateResolution;
        featureFactoryData.transperentBackground = planData.transperentBackground
        featureFactoryData.planTakenForTime = req.query.type;

        // Set reCreditDate and endDate based on subscriptionType and lifeTimeDeal
        featureFactoryData.reCreditDate = req.query.type === "monthly" ? null : Date.now() + 30 * 24 * 60 * 60 * 1000;
        featureFactoryData.endDate = false
            ? Date.now() + 36500 * 24 * 60 * 60 * 1000
            : req.query.type === "monthly"
                ? Date.now() + 30 * 24 * 60 * 60 * 1000
                : Date.now() + 365 * 24 * 60 * 60 * 1000;

        featureFactoryData.startDate = Date.now();
        await featureFactoryData.save();

        manageCacheAccounts(req.query.user);

        // NOTE: get user details and send a payment confirmation mail
        const userData = await user.findOne({ _id: req.query.user })
        sendEmail(userData.email, userData.userDetails.firstName, planData.planName, "INR" + " " + paymentInfo.data.amount / 100, paymentInfo.data.paymentInstrument.type)
        // NOTE: Redirect to confirmation page after operation on payment info is done
        res.redirect('https://www.app.creatosaurus.io/confirmation');
    } catch (error) {
        res.redirect('https://www.app.creatosaurus.io/?canceled=true');
        console.log(error)
    }
})


router.post('/callback', async (req, res) => {
    try {
        console.log("callback")
        console.log(req.body)
    } catch (error) {
        console.log(error)
    }
})



module.exports = router
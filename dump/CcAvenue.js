const express = require("express")
const router = express.Router()
const user = require("../Schema/User")
const CryptoJS = require('crypto-js');
const nodeCCAvenue = require('node-ccavenue');
const workspace = require('../Schema/Workspace')
const plan = require('../Schema/Plan')
const featureFactory = require('../Schema/UserFeatureFactory')
const transaction = null
const manageCacheAccounts = require('../Functions/ManageCacheAccounts')
const Affiliate = require("../Schema/PaymentAffiliate")
const { sendEmail } = require('../Functions/paymentConfirmEmail');
const objectId = require('mongoose').Types.ObjectId;

// DOCS: Kindly refer this url for parameters which is required for ccavenue https://dashboard.ccavenue.com/resources/integrationKit.do#billing_Page_doc
router.post('/checkout', express.json(), async (req, res) => {
    try {
        let userData = await user.findOne({ _id: req.body.userId })
        if (userData === null) return res.status(400).json({ error: "user not found" })

        const computedHashAmount = CryptoJS.SHA256(req.body.amount).toString()
        const computedHashPlanId = CryptoJS.SHA256(req.body.planId).toString()

        if (computedHashAmount !== req.body.hasm) return res.status(400).json({ error: "Invalid Request" })
        if (computedHashPlanId !== req.body.hpsl) return res.status(400).json({ error: "Invalid Request" })

        userData.userDetails = req.body.userDetails
        await userData.save()

        let access_code = "AVDQ31KK02AU84QDUA"
        let working_key = '420F9FFB3D196DA223D1837B4758F4C6'

        const ccav = new nodeCCAvenue.Configure({
            merchant_id: '3007362',
            working_key: working_key,
            access_code: access_code
        });

        const paymentData = {
            redirect_url: encodeURIComponent(
                `https://api.app.creatosaurus.io/creatosaurus/ccavenue/success?access_code=${access_code}&working_key=${working_key}&plan=${req.body.planId}&user=${req.body.userId}&type=${req.body.subscriptionType}`
            ),
            cancel_url: encodeURIComponent(`https://www.app.creatosaurus.io/?canceled=true&access_code=${access_code}&working_key=${working_key}`),
            order_id: req.body.order_id,
            currency: "INR",
            amount: req.body.amount,
            merchant_param1: req.body.originalPriceOfPlan,
            merchant_param2: req.body.tax,
            merchant_param3: req.body.couponCode,
            merchant_param4: req.body.invitationCode,
        };

        const encryptedOrder = ccav.getEncryptedOrder(paymentData);

        res.send(`https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction&access_code=${access_code}&encRequest=${encryptedOrder}`)
    } catch (error) {
        res.status(500).json({ error: "Internal server error" })
        console.log(error)
    }
})

router.post('/success', async (req, res) => {
    try {
        const { encResp } = req.body;
        const ccav = new nodeCCAvenue.Configure({
            access_code: req.query.access_code,
            working_key: req.query.working_key,
            merchant_id: '3007362',
        });

        const decryptedJsonResponse = ccav.redirectResponseToJson(encResp);
        if (decryptedJsonResponse.order_status !== "Success") return res.redirect('https://www.app.creatosaurus.io/?canceled=true');

        const obj = {
            userId: req.query.user,
            paymentGateway: "ccavenue",
            planInfo: {
                planId: req.query.plan,
                lifeTimeDeal: false,
                originalPriceOfPlan: decryptedJsonResponse.merchant_param1,
                tax: decryptedJsonResponse.merchant_param2,
                couponCode: decryptedJsonResponse.merchant_param3
            },
            paymentInfo: decryptedJsonResponse
        }

        const transactionData = await transaction.create(obj)

        const affiliateId = decryptedJsonResponse.merchant_param4
        const userId = req.query.user

        if (affiliateId != null && objectId.isValid(affiliateId)) {
            const userData = await user.findById(affiliateId)  // check the referal id of user is affiliate or not

            if (userData && userData.isAffiliate) {
                // check user current plan if free then only add to affiliate program
                const featureFactoryData = await featureFactory.findOne({ userId: userId }, { planId: 1 })
                const freePlanId = "6247087d37b553074fb6b25c"

                if (featureFactoryData.planId.toString() === freePlanId) {
                    // check affiliate user is there or not
                    const affiliateData = await Affiliate.findOne({ userId: affiliateId })

                    const userToAddData = {
                        userId: userId,
                        transactionId: transactionData._id
                    }

                    if (affiliateData !== null) {
                        affiliateData.affiliateUserAdded = [...affiliateData.affiliateUserAdded, userToAddData]
                        await affiliateData.save()
                    } else {
                        await Affiliate.create({
                            userId: affiliateId,
                            affiliateUserAdded: [userToAddData]
                        })
                    }
                }
            }
        }

        const workspaceData = await workspace.find({ user_id: req.query.user });
        const planData = await plan.findById(req.query.plan);

        // Update workspace data
        if (workspaceData) {
            const promises = workspaceData?.map(async (workspace) => {
                // Set team user state inactive
                workspace.team = workspace.team.map((member) => {
                    member.active = workspace.user_email === member.user_email;
                    return member;
                });

                // Make older workspace visible
                if (!workspace.active) {
                    workspace.active = true;
                }

                return workspace.save();
            });

            await Promise.all(promises);
        }

        // Update feature factory data
        const featureFactoryData = await featureFactory.findOne({ userId: req.query.user });

        featureFactoryData.planId = planData._id;
        featureFactoryData.workspace = planData.workspace;
        featureFactoryData.users = planData.users;
        featureFactoryData.socialAccounts = planData.socialAccounts;
        featureFactoryData.schedulePost = planData.schedulePost;

        // Handle AI credits
        featureFactoryData.aiCredites = planData.aiCredites
        featureFactoryData.hashtagSearch = planData.hashtagSearch;
        featureFactoryData.hashtagSearchInstagram = planData.hashtagSearchInstagram;
        featureFactoryData.multipleTemplateResolution = planData.multipleTemplateResolution;
        featureFactoryData.transperentBackground = planData.transperentBackground
        featureFactoryData.planTakenForTime = req.query.type;

        // Set reCreditDate and endDate based on subscriptionType and lifeTimeDeal
        featureFactoryData.reCreditDate = req.query.type === "monthly" ? null : Date.now() + 30 * 24 * 60 * 60 * 1000;
        featureFactoryData.endDate = false
            ? Date.now() + 36500 * 24 * 60 * 60 * 1000
            : req.query.type === "monthly"
                ? Date.now() + 30 * 24 * 60 * 60 * 1000
                : Date.now() + 365 * 24 * 60 * 60 * 1000;

        featureFactoryData.startDate = Date.now();
        await featureFactoryData.save();

        manageCacheAccounts(req.query.user);

        // NOTE: get user details and send a payment confirmation mail
        const userData = await user.findOne({ _id: req.query.user })
        sendEmail(userData.email, userData.userDetails.firstName, planData.planName, decryptedJsonResponse.currency + " " + decryptedJsonResponse.amount, decryptedJsonResponse.payment_mode + ":" + " " + decryptedJsonResponse.card_name)

        // NOTE: Redirect to confirmation page after operation on payment info is done
        res.redirect('https://www.app.creatosaurus.io/confirmation');
    } catch (error) {
        res.status(500).json({ error: "Internal server error" })
        console.log(error)
    }
})





module.exports = router
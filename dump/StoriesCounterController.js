const Stories = require("../Schema/StoriesCounter")

const counter = async (req, res) => {
    try {
        const story = new Stories({
            app_name: req.body.app_name,
            user_id: req.body.user_id
        })
        await story.save()
        return res.status(200).json({
            msg: "Story created successfully."
        })
    } catch (error) {
        return res.status(500).json({
            error: "Internal Server Error."
        })
    }
}


const getStoriesCount = async (req, res) => {
    try {
        let story;
        if (req.query.name !== undefined) {
            story = await Stories.count({ app_name: req.query.name })
        } else {
            story = await Stories.count({})
        }

        return res.status(200).json({
            count: story
        })
    } catch (error) {
        return res.status(500).json({
            error: "Internal Server Error."
        })
    }
}

module.exports = {
    counter,
    getStoriesCount
}
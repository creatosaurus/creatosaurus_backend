const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const compression = require('compression');
const { checkEmailSentOrNot } = require('./Functions/email');
const logger = require('./logger');
const session = require('cookie-session');
const { scheduleDailyPlanExpiryCheckAndRecredit } = require('./Functions/planExpiryAndRecredit');

// initialize tha pp
const app = express();

// connect to the data base
connectToDataBase = async () => {
  try {
    await mongoose.connect(
      'mongodb+srv://creatosaurus1:EOaVFfQ5YhOD3UhF@creatosaurus.7trc5.mongodb.net/creatosurus?authSource=admin',
      { useNewUrlParser: true, useUnifiedTopology: true }
    );

    console.log('connected to data base')
    checkEmailSentOrNot()
    scheduleDailyPlanExpiryCheckAndRecredit()
  } catch (error) {
    logger.error(error);
    process.exit(1);
  }
};

connectToDataBase();

// middle ware
app.use(cors());
app.use(express.urlencoded({ extended: true }));

app.use('/creatosaurus/stripe', require('./Routes/Stripe'))

app.use(express.json());
app.use(session({
  resave: false,
  saveUninitialized: true,
  secret: 'protectedbycreatosurus',
}));

app.use(compression());

app.use('/creatosaurus', require('./Routes/User'));
app.use('/creatosaurus/contact', require('./Routes/Contact'));
app.use('/creatosaurus/email', require('./Routes/EmailValidation'));
app.use('/creatosaurus/welcome', require('./Routes/EmailChain'));
app.use('/creatosaurus/razorpay', require('./Routes/Razorpay'))

app.use('/creatosaurus/userfeaturefactory', require('./Routes/UserFeatureSettings'));

app.use('/creatosaurus/billing-history', require('./Routes/Payment'));
app.use('/creatosaurus/workspace', require('./Routes/Workspace'));
app.use('/creatosaurus/coupon/code', require('./Routes/CouponCode'));
app.use('/creatosaurus/newsletter', require('./Routes/Newsletter'))
app.use('/creatosaurus/invoice', require('./Routes/Invoice'))
app.use('/creatosaurus/cache/notifications/workspace', require('./Routes/CacheNotification'))
app.use('/creatosaurus/location', require('./Routes/CountryStateCity'))

//v2
app.use('/creatosaurus/v2/', require('./Routes/v2/User'));
app.use('/creatosaurus/v2/workspace', require('./Routes/v2/Workspace'));
app.use('/creatosaurus/v2/delete', require('./Routes/v2/DeleteAccount'))
app.use('/creatosaurus/v2/userfeaturefactory', require('./Routes/v2/UserFeatureSettings'))

app.listen(4002, () => {
  console.log("listening on port 4002")
});

const axios = require('axios')

const verifyEmail = async (email) => {
    const apiKey = 'TN9AAQVsmmdlt2goOIKiJgRh4zZf4CWa'; // replace with your actual API key
    const url = `https://emailverifier.reoon.com/api/v1/verify?email=${email}&key=${apiKey}&mode=quick`;

    try {
        const response = await axios.get(url);
        const data = response.data;
        return data.status
    } catch (error) {
        return "valid" // if api failed then process data
    }
}

module.exports = {
    verifyEmail
}
const nodemailer = require('nodemailer');
const sesTransport = require('nodemailer-ses-transport');
const transporter = nodemailer.createTransport(
    sesTransport({
        accessKeyId: 'AKIAQ5RO3HORQD7AMD4E',
        secretAccessKey: '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',
        region: 'ap-south-1',
        rateLimit: 5,
    })
);

const sendEmail = (email, userName, planName, price, paymentMode) => {
    const mailOptions = {
        from: 'Creatosaurus <account@creatosaurus.io>',
        to: email,
        subject: 'Welcome to Creatosaurus!',
        html: `
        <!DOCTYPE html>
        <html lang="en">
        
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title></title>
            <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
            <link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet" type="text/css">
            <style type="text/css">
                @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
                @import url(https://fonts.googleapis.com/css?family=Poppins:400,700);
            </style>
        </head>
        
        <body>
            <table width="100%" border="0" class="m_-444835740439266312envelope" cellpadding="0" cellspacing="0"
                style="background-color:#eaeaea" bgcolor="#eaeaea">
                <tbody>
                    <tr>
                        <td align="center" style="background-color:#eaeaea;margin-top:0" bgcolor="#eaeaea">
                            <table align="center" border="0" class="m_-444835740439266312content" cellpadding="0"
                                cellspacing="0" style="background-color:#ffffff;width:500px" width="500" bgcolor="#ffffff">
                                <tbody>
                                    <tr>
                                        <td align="center"><a href="https://www.creatosaurus.io/" style="color:inherit"
                                                target="_blank">
                                                <table class="m_-444835740439266312image" width="100%" cellpadding="0"
                                                    cellspacing="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="m_-444835740439266312cell m_-444835740439266312content-padding"
                                                                align="left"
                                                                style="padding-left:40px;padding-right:40px;padding-top:20px">
                                                                <img src="https://s3-eu-west-1.amazonaws.com/topolio/uploads/634e334873887/1666069362.jpg"
                                                                    alt="Creatosaurus" width="80" border="0"
                                                                    style="border:none;outline:none;border-collapse:collapse;display:block;border-style:none"
                                                                    class="CToWUd" data-bit="iit">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </a>
                                            <table align="left" width="100%" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                    <tr>
                                                        <td align="left" class="m_-444835740439266312content-padding"
                                                            style="padding-left:40px;padding-right:40px;font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:700;font-size:36px;line-height:43px;letter-spacing:-1px;padding-top:20px;color:#221f1f">
                                                            Welcome</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table align="left" width="100%" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                    <tr>
                                                        <td align="left" class="m_-444835740439266312content-padding"
                                                            style="padding-left:40px;padding-right:40px;font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:400;font-size:16px;line-height:21px;padding-top:20px;color:#221f1f">
                                                            Hi <span style="word-break:break-all">${userName}</span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table align="left" width="100%" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                    <tr>
                                                        <td align="left" class="m_-444835740439266312content-padding"
                                                            style="padding-left:40px;padding-right:40px;font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:400;font-size:16px;line-height:21px;padding-top:20px;color:#221f1f">
                                                            You're ready to use The Best All-In-One Social Media Tool</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="font-size:0;line-height:0;height:20px" height="20">&nbsp;
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                    <tr>
                                                        <td class="m_-444835740439266312content-padding"
                                                            style="padding-left:40px;padding-right:40px">
                                                            <table class="m_-444835740439266312color-wrapper" width="100%"
                                                                cellpadding="0" cellspacing="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td bgcolor="#FFFFFF"
                                                                            style="border-radius:8px;padding-bottom:0;border:1px solid #eaeaea">
                                                                            <table align="left" width="100%" cellpadding="0"
                                                                                cellspacing="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td align="left"
                                                                                            class="m_-444835740439266312content-padding"
                                                                                            style="padding-top:20px;color:#221f1f;font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:300;font-size:20px;line-height:24px;padding-left:20px;padding-right:20px">
                                                                                            Your Account Information:</td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="padding-top:20px">
                                                                                            <table align="left" width="100%"
                                                                                                cellpadding="0" cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td align="left"
                                                                                                            class="m_-444835740439266312content-padding"
                                                                                                            style="font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:700;font-size:14px;line-height:17px;letter-spacing:-0.2px;color:#221f1f;padding-left:20px;padding-right:20px;padding-top:0">
                                                                                                            Email</td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <table align="left" width="100%"
                                                                                                cellpadding="0" cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td align="left"
                                                                                                            class="m_-444835740439266312content-padding"
                                                                                                            style="font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:400;font-size:16px;line-height:21px;color:#221f1f;padding-left:20px;padding-right:20px;padding-top:8px">
                                                                                                            <a href="mailto:${email}"
                                                                                                                target="_blank">${email}</a>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="padding-top:20px">
                                                                                            <table align="left" width="100%"
                                                                                                cellpadding="0" cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td align="left"
                                                                                                            class="m_-444835740439266312content-padding"
                                                                                                            style="font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:700;font-size:14px;line-height:17px;letter-spacing:-0.2px;color:#221f1f;padding-left:20px;padding-right:20px;padding-top:0">
                                                                                                            Company Name</td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <table align="left" width="100%"
                                                                                                cellpadding="0" cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td align="left"
                                                                                                            class="m_-444835740439266312content-padding"
                                                                                                            style="font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:400;font-size:16px;line-height:21px;color:#221f1f;padding-left:20px;padding-right:20px;padding-top:8px">
                                                                                                            Creatosaurus Private
                                                                                                            Limited
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="padding-top:20px">
                                                                                            <table align="left" width="100%"
                                                                                                cellpadding="0" cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td align="left"
                                                                                                            class="m_-444835740439266312content-padding"
                                                                                                            style="font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:700;font-size:14px;line-height:17px;letter-spacing:-0.2px;color:#221f1f;padding-left:20px;padding-right:20px;padding-top:0">
                                                                                                            Plan</td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <table align="left" width="100%"
                                                                                                cellpadding="0" cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td align="left"
                                                                                                            class="m_-444835740439266312content-padding"
                                                                                                            style="font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:400;font-size:16px;line-height:21px;color:#221f1f;padding-left:20px;padding-right:20px;padding-top:8px">
                                                                                                            ${planName}<br>${price}
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="padding-top:20px">
                                                                                            <table align="left" width="100%"
                                                                                                cellpadding="0" cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td align="left"
                                                                                                            class="m_-444835740439266312content-padding"
                                                                                                            style="font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:700;font-size:14px;line-height:17px;letter-spacing:-0.2px;color:#221f1f;padding-left:20px;padding-right:20px;padding-top:0">
                                                                                                            Payment</td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <table align="left" width="100%"
                                                                                                cellpadding="0" cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td align="left"
                                                                                                            class="m_-444835740439266312content-padding"
                                                                                                            style="font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:400;font-size:16px;line-height:21px;color:#221f1f;padding-left:20px;padding-right:20px;padding-top:8px">
                                                                                                            <table
                                                                                                                cellpadding="0"
                                                                                                                cellspacing="0">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td style='text-transform:uppercase;'>
                                                                                                                        ${paymentMode}
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="font-size:0;line-height:0;height:20px"
                                                                                            height="20">&nbsp;</td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table align="left" width="100%" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                    <tr>
                                                        <td align="left" class="m_-444835740439266312content-padding"
                                                            style="padding-left:40px;padding-right:40px;font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:400;font-size:16px;line-height:21px;padding-top:20px;color:#221f1f">
                                                            We're here to help if you need it. Visit the <a
                                                                href="https://tawk.to/chat/615fde58157d100a41ab5e5d/1fhf7p2ht"
                                                                style="color:inherit;text-decoration:underline"
                                                                target="_blank">Help
                                                                Centre</a> for more info or <a
                                                                href="https://creatosaurus.io/contact"
                                                                style="color:inherit;text-decoration:underline"
                                                                target="_blank">contact
                                                                us</a>.</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table align="left" width="100%" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                    <tr>
                                                        <td align="left" class="m_-444835740439266312content-padding"
                                                            style="padding-left:40px;padding-right:40px;font-size:14px;line-height:17px;letter-spacing:-0.2px;font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:700;padding-top:20px;color:#221f1f">
                                                            The Creatosaurus team</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                    <tr>
                                                        <td class="m_-444835740439266312content-padding"
                                                            style="padding-left:40px;padding-right:40px;padding-top:30px">
                                                            <table align="center" width="100%" cellpadding="0" cellspacing="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td
                                                                            style="font-size:0;line-height:0;border-style:solid;border-bottom-width:0;border-color:#221f1f;border-top-width:2px">
                                                                            &nbsp;</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="m_-444835740439266312footer-shell"
                                            style="background-color:#ffffff" bgcolor="#ffffff">
                                            <table class="m_-444835740439266312footer" width="100%" border="0" cellpadding="0"
                                                cellspacing="0">
                                                <tbody>
                                                    <tr>
                                                        <td align="center" valign="top"
                                                            class="m_-444835740439266312footer-shell m_-444835740439266312content-padding"
                                                            style="padding-left:40px;padding-right:40px;background-color:#ffffff"
                                                            bgcolor="#ffffff">
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="font-size:0;line-height:0;height:40px"
                                                                            height="40">&nbsp;</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td valign="top" style="padding:0 20px 0 0">
                                                                            <table
                                                                                class="m_-444835740439266312component-image m_-444835740439266312image"
                                                                                width="100%" cellpadding="0" cellspacing="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td class="m_-444835740439266312cell m_-444835740439266312component-image"
                                                                                            align="center"
                                                                                            style="padding-top:0">
                                                                                            <img src="https://s3-eu-west-1.amazonaws.com/topolio/uploads/634e334873887/1666069362.jpg"
                                                                                                alt="" width="50" border="0"
                                                                                                style="border:none;outline:none;border-collapse:collapse;display:block"
                                                                                                class="CToWUd" data-bit="iit">
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td valign="top">
                                                                            <table width="100%" valign="top" cellpadding="0"
                                                                                cellspacing="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table align="left" width="100%"
                                                                                                cellpadding="0" cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td align="left"
                                                                                                            style="font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:400;letter-spacing:-0.1px;color:#a4a4a4;font-size:15px;line-height:19px;padding-top:0">
                                                                                                            <span
                                                                                                                style="color:#4d4d4d">By
                                                                                                                joining
                                                                                                                Creatosaurus,
                                                                                                                you've agreed to
                                                                                                                our
                                                                                                                <a href="https://www.creatosaurus.io/terms"
                                                                                                                    style="text-decoration:underline;color:#4d4d4d"
                                                                                                                    target="_blank">
                                                                                                                    Terms
                                                                                                                    of Use
                                                                                                                </a>
                                                                                                                and
                                                                                                                <a href="https://www.creatosaurus.io/privacy"
                                                                                                                    style="text-decoration:underline;color:#4d4d4d"
                                                                                                                    target="_blank">
                                                                                                                    Privacy
                                                                                                                    Statement</a>.<br><br>Please
                                                                                                                review the <a
                                                                                                                    href="https://www.creatosaurus.io/privacy"
                                                                                                                    style="text-decoration:underline;color:#4d4d4d"
                                                                                                                    target="_blank">
                                                                                                                    Privacy
                                                                                                                    Statement</a>
                                                                                                                to
                                                                                                                learn how we use
                                                                                                                your personal
                                                                                                                information and
                                                                                                                go
                                                                                                                to your
                                                                                                                <a href="https://app.creatosaurus.io/"
                                                                                                                    style="text-decoration:underline;color:#4d4d4d"
                                                                                                                    target="_blank">
                                                                                                                    Account
                                                                                                                    Settings</a>
                                                                                                                for
                                                                                                                more details
                                                                                                                about
                                                                                                                plan and
                                                                                                                features.</span>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <table width="100%" cellpadding="0"
                                                                                                cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td style="font-size:0;line-height:0;height:20px"
                                                                                                            height="20">&nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <table align="left" width="100%"
                                                                                                class="m_-444835740439266312address"
                                                                                                cellpadding="0" cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td align="left"
                                                                                                            style="font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:400;font-size:11px;line-height:14px;letter-spacing:-0.1px;color:#a4a4a4;padding-top:0">
                                                                                                            <span
                                                                                                                class="m_-444835740439266312hide-link"
                                                                                                                style="text-decoration:none;color:#a4a4a4;">
                                                                                                                Creatosaurus
                                                                                                                Private
                                                                                                                Limited
                                                                                                            </span>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <table align="left" width="100%"
                                                                                                cellpadding="0" cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td align="left"
                                                                                                            style="font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:400;font-size:12px;line-height:15px;letter-spacing:-0.12px;padding-top:20px;color:#a9a6a6">
                                                                                                            <a class="m_-444835740439266312footer-link"
                                                                                                                href="https://www.creatosaurus.io/terms"
                                                                                                                style="text-decoration:underline;line-height:20px;color:#a4a4a4"
                                                                                                                target="_blank">
                                                                                                                Terms
                                                                                                                of
                                                                                                                Use
                                                                                                            </a>
                                                                                                            <br>
                                                                                                            <a class="m_-444835740439266312footer-link"
                                                                                                                href="https://www.creatosaurus.io/privacy"
                                                                                                                style="text-decoration:underline;line-height:20px;color:#a4a4a4"
                                                                                                                target="_blank">
                                                                                                                Privacy
                                                                                                            </a>
                                                                                                            <br>
                                                                                                            <a class="m_-444835740439266312footer-link"
                                                                                                                href="https://tawk.to/chat/615fde58157d100a41ab5e5d/1fhf7p2ht"
                                                                                                                style="text-decoration:underline;line-height:20px;color:#a4a4a4"
                                                                                                                target="_blank">
                                                                                                                Help
                                                                                                                Centre
                                                                                                            </a>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <table align="left" width="100%"
                                                                                                cellpadding="0" cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td align="left"
                                                                                                            style="font-family:Poppins,Helvetica,Roboto,Segoe UI,sans-serif;font-weight:400;font-size:11px;line-height:14px;letter-spacing:-0.1px;padding-top:20px;color:#a4a4a4">
                                                                                                            This message was
                                                                                                            emailed
                                                                                                            to <a
                                                                                                                href="https://www.creatosaurus.io/"
                                                                                                                class="m_-444835740439266312hide-link"
                                                                                                                style="color:#a4a4a4;text-decoration:none"
                                                                                                                target="_blank">[${email}]</a>
                                                                                                            by Creatosaurus as
                                                                                                            part
                                                                                                            of
                                                                                                            your
                                                                                                            Creatosaurus&nbsp;membership.
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <table width="100%" cellpadding="0"
                                                                                                cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td style="font-size:0;line-height:0;height:40px"
                                                                                                            height="40">&nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </body>
        
        </html>
     `,
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error)
        }
    });
};


module.exports = {
    sendEmail
}
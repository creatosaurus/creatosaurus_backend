const axios = require("axios")
const featureFactory = require('../Schema/UserFeatureSettings')
const workspace = require('../Schema/Workspace')

let retryCount = 0
const manageCacheAccounts = async (userId) => {
    try {
        const workspaceData = await workspace.find({ user_id: userId })
        const featureFactoryData = await featureFactory.findOne({ userId: userId })

        let accounts = []
        for await (const data of workspaceData) {
            const res = await axios.get(`https://api.cache.creatosaurus.io/cache/accounts/user/workspace/accounts/${data._id}`)
            accounts = [...accounts, ...res.data]
        }

        // if social accounts are unlimited then don't need to manage queue
        if (featureFactoryData.socialAccounts === "unlimited") {
            accounts = accounts.map((data) => {
                data.active = true
                return data
            })

            await axios.post("https://api.cache.creatosaurus.io/cache/accounts/user/workspace/add/accounts", {
                accounts: accounts
            })
            return
        }

        let workspaceIdArray = workspaceData.map(data => {
            return data._id
        })

        // if connected account length is less than or equal to social connected account length
        if (accounts.length <= parseInt(featureFactoryData.socialAccounts)) {
            accounts = accounts.map((data) => {
                data.active = true
                return data
            })

            let dataCount = await axios.post("https://api.cache.creatosaurus.io/cache/accounts/user/workspace/accounts/queue/management/plan/expire", {
                workspaceIdArray: workspaceIdArray,
                limit: parseInt(featureFactoryData.schedulePost)
            })

            featureFactoryData.schedulePost = dataCount.data.count
            featureFactoryData.socialAccounts = parseInt(featureFactoryData.socialAccounts) - accounts.length
            await featureFactoryData.save()
            await axios.post("https://api.cache.creatosaurus.io/cache/accounts/user/workspace/add/accounts", {
                accounts: accounts
            })
            return
        }

        // deactive the other accounts
        accounts = accounts.map((data, index) => {
            if (index < parseInt(featureFactoryData.socialAccounts)) {
                data.active = true
            } else {
                data.active = false
            }
            return data
        })

        let dataCount = await axios.post("https://api.cache.creatosaurus.io/cache/accounts/user/workspace/accounts/queue/management/plan/expire", {
            workspaceIdArray: workspaceIdArray,
            limit: parseInt(featureFactoryData.schedulePost)
        })

        await axios.post("https://api.cache.creatosaurus.io/cache/accounts/user/workspace/add/accounts", {
            accounts: accounts
        })

        featureFactoryData.schedulePost = dataCount.data.count
        featureFactoryData.socialAccounts = 0
        await featureFactoryData.save()
    } catch (error) {
        retryCount = retryCount + 1
        if (retryCount < 5) {
            manageCacheAccounts(userId)
        }
    }
}

module.exports = manageCacheAccounts
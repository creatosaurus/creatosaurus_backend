const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Plan = new Schema({
    planName: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        trim: true
    },
    monthlyPriceINR: Number,
    yearlyPriceINR: Number,
    monthlyPriceUSD: Number,
    yearlyPriceUSD: Number,
    workspace: String,
    brandKit: String,
    users: String,
    socialAccounts: String,
    schedulePost: String,
    aiCredites: String,
    aiImageCredits: String,
    storage: Number
});

module.exports = mongoose.model('Plan', Plan);
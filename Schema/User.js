const mongoose = require('mongoose');
const schema = mongoose.Schema;

const User = new schema(
  {
    email: {
      type: String,
      trim: true,
      lowercase: true,
      unique: true,
      required: 'Email address is required',
    },
    firstName: {
      type: String,
      lowercase: true
    },
    lastName: {
      type: String,
      lowercase: true
    },
    describleYourself: String,
    find_out_about_creatosaurus: String,
    active_workspace: String,
    active_workspace_name: String,
    onboarding_status: {
      type: Boolean,
      default: false
    },
    userName: {
      type: String,
      trim: true
    },
    password: {
      type: String,
      trim: true,
    },
    mobile: Number,
    isAdmin: {
      type: Boolean,
      default: false,
    },
    isAffiliate: {
      type: Boolean,
      default: false,
    },
    emailVerified: {
      type: Boolean,
      default: false,
    },
    accountActivated: {
      type: Boolean,
      default: false,
    },
    timeZone: {
      type: String,
      default: 'Asia/Calcutta',
    },
    accountPaymentStatus: {
      type: String,
      default: 'free',
    },
    resetPasswordToken: String,
    resetPasswordExpires: String,
    freeTrial: Boolean,
    authType: String,
    failedLoginAttempts: {
      type: Number,
      default: 0
    },
    sendEmail: Boolean,
    verifyCode: String,
    taskCompleted: Boolean,
    malavEmailSent: {
      type: Boolean,
      default: false
    },
    mayurEmailSent: {
      type: Boolean,
      default: false
    },
    malav2EmailSent: {
      type: Boolean,
      default: false
    },
    userDetails: {
      type: Object
    }
  },
  { timestamps: true });

module.exports = mongoose.model('User', User);

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let waitList = new Schema({
    userId: String,
    userWaitListNumber: Number
});

module.exports = mongoose.model('WaitList', waitList);
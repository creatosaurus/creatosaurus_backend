const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AffiliateSchema = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'User' },
    affiliateUserAdded: [{
        userId: { type: Schema.Types.ObjectId, ref: 'User' },
        transactionId: { type: Schema.Types.ObjectId, ref: 'Transaction' }
    }]
});
 
module.exports = mongoose.model('PaymentAffiliate', AffiliateSchema);

const mongoose = require('mongoose');
const schema = mongoose.Schema;

const Afilliate = new schema({
    userId: { type: schema.Types.ObjectId, ref: 'User' },
    afilliateUserAdded: [{ type: schema.Types.ObjectId, ref: 'User' }]
})

Afilliate.index({ userId: 1, "afilliateUserAdded.createdAt": 1 });

module.exports = mongoose.model('afilliate', Afilliate);

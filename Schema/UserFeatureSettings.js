const mongoose = require('mongoose');
const { Schema } = mongoose;

const userFeatureSettingsSchema = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'User', required: true, unique: true },
    planId: { type: Schema.Types.ObjectId, ref: 'Plan', required: true },

    // Feature limits and allocations for the user
    workspace: Number,
    brandKit: Number,
    users: Number,
    socialAccounts: Number,
    schedulePost: Number,
    aiCredits: Number,
    aiImageCredits: Number,
    storage: Number,

    // Dates related to plan and credit management
    reCreditDate: Date,
    startDate: Date,
    endDate: Date,
}, { timestamps: true });

module.exports = mongoose.model('UserFeatureSettings', userFeatureSettingsSchema);
